<?php

use ProjectBoston\CustomerServiceCenter\Application\AppKernel;
use Symfony\Component\HttpFoundation\Request;

$loader = require __DIR__.'/../app/autoload.php';
require __DIR__.'/../app/bootstrap.php.cache';

// Use APC for autoloading to improve performance.
// Change 'sf2' to a unique prefix in order to prevent cache key conflicts
// with other applications also using APC.
/*
$loader = new ApcClassLoader('sf2', $loader);
$loader->register(true);
*/

if (!isset($_SERVER['SYMFONY_ENV'])) {
    $_SERVER['SYMFONY_ENV'] = 'prod';
}
switch ($_SERVER['SYMFONY_ENV']){
    case 'dev':
        $debug = true;
        $environment =  'dev';
        break;
    default:
        $debug = false;
        $environment = 'prod';
}

$kernel = new AppKernel($environment, $debug);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
