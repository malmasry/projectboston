<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Repository;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Review\Repository\ReviewableRepository;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
interface CustomerRepository extends ReviewableRepository
{

    /**
     * @param int $id
     *
     * @return Customer[]
     */
    public function findAllDeletableCustomer($id);

    /**
     * @param int $id
     *
     * @return Customer[]
     */
    public function findAllAncestors($id);

    /**
     * @param int $id
     * @param int $parentId
     *
     * @return Customer[]
     */
    public function findAllAncestorsWithParentFilter($id, $parentId);

    /**
     * @param int $id
     *
     * @return Customer
     */
    public function findById($id);
}
