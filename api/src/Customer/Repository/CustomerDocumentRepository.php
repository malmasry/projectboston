<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class CustomerDocumentRepository extends DocumentRepository implements CustomerRepository
{
    /**
     * @param mixed $id
     *
     * @return ReviewableInterface
     */
    public function findReviewableObjectById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }


    /**
     * @param int $id
     *
     * @return Customer[]
     */
    public function findAllDeletableCustomer($id)
    {
        $qb = $this->createQueryBuilder();

        return $qb
            ->find()
            ->addOr($qb->expr()->field('ancestors.id')->equals($id))
            ->addOr($qb->expr()->field('id')->equals($id))
            ->getQuery()
            ->execute()
            ->toArray(false);
    }

    /**
     * @param int $id
     *
     * @return Customer[]
     */
    public function findAllAncestors($id)
    {
        return $this->findBy(['ancestors.id' => $id]);
    }

    /**
     * @param int $id
     * @param int $parentId
     *
     * @return Customer[]
     */
    public function findAllAncestorsWithParentFilter($id, $parentId)
    {
        return $this->findBy(['ancestors.id' => $id, 'parent.id' => $parentId]);
    }

    /**
     * @inheritDoc
     */
    public function findById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }
}
