<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\CacheWarmer;

use Doctrine\ODM\MongoDB\Types\Type;
use ProjectBoston\CustomerServiceCenter\Customer\Doctrine\MongoDB\Types\PhoneNumberType;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class MongoDBCacheWarmer implements CacheWarmerInterface
{
    /**
     * This cache warmer is not optional, without the custom types exceptions occurs!
     *
     * @return false
     */
    public function isOptional()
    {
        return false;
    }

    /**
     * Warms up the cache.
     *
     * @param string $cacheDir The cache directory
     */
    public function warmUp($cacheDir)
    {
        Type::registerType('phoneNumber', PhoneNumberType::class);
    }
}
