<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Controller;

use ProjectBoston\CustomerServiceCenter\Customer\CustomerTransitions;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Review\Model\Review;
use ProjectBoston\CustomerServiceCenter\Review\Model\ObjectReview;
use SM\Factory\FactoryInterface;
use SM\SMException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Delete
{
    private $sm;

    /**
     * @param FactoryInterface      $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;

    }

    /**
     * @param Customer $customer
     * @param Review   $review
     *
     * @throws SMException
     *
     * @return Response
     */
    public function __invoke(Customer $customer, Review $review)
    {
        $this->sm->get(new ObjectReview($customer, $review), CustomerTransitions::GRAPH)
            ->apply(CustomerTransitions::CREATE_DELETE);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
