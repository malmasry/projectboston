<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\Customer\CustomerTransitions;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use SM\Factory\FactoryInterface;
use SM\SMException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Create
{
    private $sm;
    private $dm;

    /**
     * @param FactoryInterface $sm
     * @param ObjectManager    $dm
     */
    public function __construct(FactoryInterface $sm, ObjectManager $dm)
    {
        $this->sm = $sm;
        $this->dm = $dm;
    }

    /**
     * @param Customer $customer
     *
     * @throws SMException
     *
     * @return Response
     */
    public function __invoke(Customer $customer)
    {
        $this->dm->persist($customer);

        $this->sm->get($customer, CustomerTransitions::GRAPH_NEW)
            ->apply(CustomerTransitions::CREATE_NEW);

        $this->dm->flush();

        return new Response('', Response::HTTP_CREATED);

    }
}
