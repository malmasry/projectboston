<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Update
{
    private $dm;

    /**
     * @param ObjectManager $dm
     */
    public function __construct(ObjectManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param Customer $customer
     *
     * @return Response
     */
    public function __invoke(Customer $customer)
    {
        $this->dm->persist($customer);
        $this->dm->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
