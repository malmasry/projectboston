<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Controller;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Customer\Repository\CustomerRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class All
{
    private $tokenStorage;
    private $customerRepository;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param CustomerRepository    $customerRepository
     */
    public function __construct(TokenStorageInterface $tokenStorage, CustomerRepository $customerRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param int $parent
     *
     * @return Customer[]
     */
    public function __invoke($parent)
    {
        $ancestorsId = $this->tokenStorage->getToken()->getUser()->getCustomer()->getId();

        if ($parent === null) {
            $customers = $this->customerRepository->findAllAncestors($ancestorsId);
            $customers[] = $this->tokenStorage->getToken()->getUser()->getCustomer();

            return $customers;
        }

        return $this->customerRepository->findAllAncestorsWithParentFilter($ancestorsId, $parent);
    }
}
