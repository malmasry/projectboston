<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Controller;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Get
{
    /**
     * @param Customer $customer
     *
     * @return Customer
     */
    public function __invoke(Customer $customer)
    {
        return $customer;

    }
}
