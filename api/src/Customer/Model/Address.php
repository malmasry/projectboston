<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 *
 * @ODM\EmbeddedDocument
 * @Gedmo\Loggable
 */
class Address
{
    /**
     * @ODM\String(name="s")
     * @Gedmo\Versioned
     */
    private $street;

    /**
     * @ODM\String(name="z")
     * @Gedmo\Versioned
     */
    private $zip;

    /**
     * @ODM\String(name="c")
     * @Gedmo\Versioned
     */
    private $city;

    /**
     * @ODM\String(name="cc")
     * @Gedmo\Versioned
     */
    private $country;

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }
}
