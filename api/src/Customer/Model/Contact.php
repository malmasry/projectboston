<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use ProjectBoston\CustomerServiceCenter\Customer\Doctrine\MongoDB\Annotations\PhoneNumber;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 *
 * @ODM\EmbeddedDocument
 * @Gedmo\Loggable
 */
class Contact
{
    /**
     * @ODM\String(name="fn")
     * @Gedmo\Versioned
     */
    private $firstName;

    /**
     * @ODM\String(name="ln")
     * @Gedmo\Versioned
     */
    private $lastName;

    /**
     * @ODM\String(name="e")
     * @Gedmo\Versioned
     */
    private $email;

    /**
     * @ODM\String(name="t")
     * @Gedmo\Versioned
     */
    private $title;

    /**
     * @ODM\String(name="s")
     * @Gedmo\Versioned
     */
    private $salutation;

    /**
     * @PhoneNumber
     * @Gedmo\Versioned
     */
    private $phone;

    /**
     * @PhoneNumber
     * @Gedmo\Versioned
     */
    private $fax;

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param \libphonenumber\PhoneNumber $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return \libphonenumber\PhoneNumber
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param \libphonenumber\PhoneNumber $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return \libphonenumber\PhoneNumber
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
