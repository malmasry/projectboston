<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use ProjectBoston\CustomerServiceCenter\Customer\CustomerTransitions;
use ProjectBoston\CustomerServiceCenter\Customer\Doctrine\MongoDB\Annotations\PhoneNumber;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableDocumentTrait;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 *
 * @ODM\Document(
 *  collection="customer",
 *  repositoryClass="ProjectBoston\CustomerServiceCenter\Customer\Repository\CustomerDocumentRepository"
 * )
 * @Gedmo\Loggable(logEntryClass="ProjectBoston\CustomerServiceCenter\Core\Model\LogEntry")
 */
class Customer implements ReviewableInterface
{
    const TYPE_ADMIN    = 'admin';
    const TYPE_PARTNER  = 'partner';
    const TYPE_CONSUMER = 'consumer';

    use ReviewableDocumentTrait;

    /**
     * @ODM\Id(strategy="INCREMENT")
     */
    private $id;

    /**
     * @ODM\String(name="n")
     * @Gedmo\Versioned
     */
    private $name;

    /**
     * @ODM\String(name="t")
     * @Gedmo\Versioned
     */
    private $type;

    /**
     * @ODM\EmbedOne(targetDocument="ProjectBoston\CustomerServiceCenter\Customer\Model\Contact")
     * @Gedmo\Versioned
     */
    private $contact;

    /**
     * @ODM\EmbedOne(targetDocument="ProjectBoston\CustomerServiceCenter\Customer\Model\Address")
     * @Gedmo\Versioned
     */
    private $address;

    /**
     * @PhoneNumber
     * @Gedmo\Versioned
     */
    private $phone;

    /**
     * @PhoneNumber
     * @Gedmo\Versioned
     */
    private $fax;

    /**
     * @ODM\ReferenceMany(targetDocument="ProjectBoston\CustomerServiceCenter\Customer\Model\Customer", name="a")
     * @ODM\Index
     */
    private $ancestors = [];

    /**
     * @ODM\ReferenceOne(targetDocument="ProjectBoston\CustomerServiceCenter\Customer\Model\Customer", name="p")
     * @ODM\Index
     */
    private $parent;

    public function __construct()
    {
        $this->address = new Address();
        $this->setReviewStatus(CustomerTransitions::NONE);
    }

    /**
     * @param Customer[] $ancestors
     */
    public function setAncestors($ancestors)
    {
        $this->ancestors = $ancestors;
    }

    /**
     * @return Customer[]
     */
    public function getAncestors()
    {
        return $this->ancestors;
    }

    /**
     * @param Customer $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return Customer
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param \libphonenumber\PhoneNumber $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return \libphonenumber\PhoneNumber
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param \libphonenumber\PhoneNumber $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return \libphonenumber\PhoneNumber
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function __toString()
    {
        return (string) $this->id;
    }
}
