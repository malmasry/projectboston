<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Security;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Customer\Repository\CustomerRepository;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class CustomerAccessDecisionMaker
{
    private $customerRepository;

    /**
     * @param CustomerRepository $customerRepository
     */
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param Customer $currentCustomer
     * @param int      $customerId
     *
     * @return boolean
     */
    public function hasAccess(Customer $currentCustomer, $customerId)
    {
        $currentCustomerId = $currentCustomer->getId();

        if ($customerId === null || $currentCustomerId === (int) $customerId) {
            return true;
        }

        $customer = $this->customerRepository->findById($customerId);

        if ($customer === null) {
            return false;
        }

        foreach ($customer->getAncestors() as $ancestor) {
            if ($currentCustomerId === $ancestor->getId()) {
                return true;
            }
        }

        return false;
    }
}
