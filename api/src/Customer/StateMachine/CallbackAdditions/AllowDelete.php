<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\StateMachine\CallbackAdditions;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\Customer\Repository\CustomerRepository;
use ProjectBoston\CustomerServiceCenter\Review\Model\ObjectReview;
use ProjectBoston\CustomerServiceCenter\Review\StateMachine\CallbackAdditions\AllowCallback;
use ProjectBoston\CustomerServiceCenter\User\Repository\UserRepository;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class AllowDelete implements AllowCallback
{
    private $dm;
    private $customerRepository;
    private $userRepository;

    /**
     * @param CustomerRepository $customerRepository
     * @param UserRepository     $userRepository
     * @param ObjectManager      $dm
     */
    public function __construct(
        CustomerRepository $customerRepository,
        UserRepository $userRepository,
        ObjectManager $dm
    ) {
        $this->dm = $dm;
        $this->customerRepository = $customerRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param ObjectReview $objectReview
     */
    public function allow(ObjectReview $objectReview)
    {
        $affectedCustomers = $this->customerRepository->findAllDeletableCustomer($objectReview->getObject()->getId());

        $affectedCustomerIds = [];
        foreach ($affectedCustomers as $affectedCustomer) {
            $affectedCustomerIds[] = $affectedCustomer->getId();
            $this->dm->remove($affectedCustomer);
        }
        $users = $this->userRepository->findAllUsersByCustomerIds($affectedCustomerIds);

        foreach ($users as $user) {
            $this->dm->remove($user);
        }

        $this->dm->flush();
    }
}
