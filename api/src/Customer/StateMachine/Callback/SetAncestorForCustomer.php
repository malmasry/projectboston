<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\StateMachine\Callback;

use Doctrine\Common\Collections\Collection;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class SetAncestorForCustomer
{
    /**
     * @param Customer $customer
     */
    public function execute(Customer $customer)
    {
        if ($customer->getParent() instanceof Customer) {

            $ancestors = $customer->getParent()->getAncestors();
            if ($ancestors instanceof Collection) {
                $ancestors = $ancestors->toArray();
            }
            $ancestors[] = $customer->getParent();
            $customer->setAncestors($ancestors);
        }
    }
}
