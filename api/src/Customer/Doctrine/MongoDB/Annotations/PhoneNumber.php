<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Doctrine\MongoDB\Annotations;

use Doctrine\ODM\MongoDB\Mapping\Annotations\AbstractField;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 * @Annotation
 */
class PhoneNumber extends AbstractField
{
    public $type = 'phoneNumber';
}
