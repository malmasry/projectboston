<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Doctrine\MongoDB\Types;

use Doctrine\ODM\MongoDB\Types\Type;
use libphonenumber\PhoneNumber;
use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberUtil;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class PhoneNumberType extends Type
{
    /**
     * Converts a value from its database representation to its PHP representation
     * of this type.
     *
     * @param string $value The value to convert.
     *
     * @return PhoneNumber The PHP representation of the value.
     */
    public function convertToPHPValue($value)
    {
        if (null === $value) {
            return null;
        }

        $util = PhoneNumberUtil::getInstance();

        return $util->parse($value, PhoneNumberUtil::UNKNOWN_REGION);
    }

    /**
     * Converts a value from its database representation to its PHP representation
     * of this type.
     *
     * @return string
     */
    public function closureToPHP()
    {
        return '$return = \libphonenumber\PhoneNumberUtil::getInstance()->parse($value, \libphonenumber\PhoneNumberUtil::UNKNOWN_REGION);';
    }

    /**
     * Converts a value from its PHP representation to its database representation
     * of this type.
     *
     * @param PhoneNumber|string $value The value to convert.
     *
     * @return string The database representation of the value.
     */
    public function convertToDatabaseValue($value)
    {
        if (null === $value) {
            return null;
        }

        $util = PhoneNumberUtil::getInstance();

        if (is_string($value)) {
            $value = $util->parse($value, PhoneNumberUtil::UNKNOWN_REGION);
        }

        return $util->format($value, PhoneNumberFormat::E164);
    }
}
