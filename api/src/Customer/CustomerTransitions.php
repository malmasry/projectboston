<?php

namespace ProjectBoston\CustomerServiceCenter\Customer;

use ProjectBoston\CustomerServiceCenter\Core\Transitions;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class CustomerTransitions extends Transitions
{
    const GRAPH = 'csc_customer';

    const GRAPH_NEW = 'csc_customer_new';
}
