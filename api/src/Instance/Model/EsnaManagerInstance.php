<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @ODM\Document(repositoryClass="ProjectBoston\CustomerServiceCenter\Instance\Repository\InstanceDocumentRepository")
 * @Gedmo\Loggable(logEntryClass="ProjectBoston\CustomerServiceCenter\Core\Model\LogEntry")
 * @codeCoverageIgnore
 */
class EsnaManagerInstance extends Instance
{
    /**
     * @Gedmo\Versioned
     * @ODM\String(name="bu")
     */
    private $baseUrl;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="au")
     */
    private $authUrl;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="aci")
     */
    private $authClientId;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="acs")
     */
    private $authClientSecret;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="aun")
     */
    private $authUsername;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="apw")
     */
    private $authPassword;

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    /**
     * @return string
     */
    public function getAuthUrl()
    {
        return $this->authUrl;
    }

    /**
     * @param string $authUrl
     */
    public function setAuthUrl($authUrl)
    {
        $this->authUrl = $authUrl;
    }

    /**
     * @return string
     */
    public function getAuthClientId()
    {
        return $this->authClientId;
    }

    /**
     * @param string $authClientId
     */
    public function setAuthClientId($authClientId)
    {
        $this->authClientId = $authClientId;
    }

    /**
     * @return string
     */
    public function getAuthClientSecret()
    {
        return $this->authClientSecret;
    }

    /**
     * @param string $authClientSecret
     */
    public function setAuthClientSecret($authClientSecret)
    {
        $this->authClientSecret = $authClientSecret;
    }

    /**
     * @return string
     */
    public function getAuthUsername()
    {
        return $this->authUsername;
    }

    /**
     * @param string $authUsername
     */
    public function setAuthUsername($authUsername)
    {
        $this->authUsername = $authUsername;
    }

    /**
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->authPassword;
    }

    /**
     * @param string $authPassword
     */
    public function setAuthPassword($authPassword)
    {
        $this->authPassword = $authPassword;
    }
}
