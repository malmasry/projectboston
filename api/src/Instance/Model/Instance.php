<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use ProjectBoston\CustomerServiceCenter\Instance\InstanceTransitions;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableDocumentTrait;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableInterface;
use ProjectBoston\CustomerServiceCenter\Core\Model\DeactivatableInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @ODM\DiscriminatorMap({
 *  "esna_instance" = "ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstanceProInstance"
 * })
 * @ODM\DiscriminatorField("type")
 * @ODM\DefaultDiscriminatorValue("esna_instance")
 * @ODM\Document(
 *  collection="instance",
 *  repositoryClass="ProjectBoston\CustomerServiceCenter\Instance\Repository\InstanceDocumentRepository"
 * )
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @Gedmo\Loggable(logEntryClass="ProjectBoston\CustomerServiceCenter\Core\Model\LogEntry")
 *
 * @codeCoverageIgnore
 */
class Instance implements ReviewableInterface, DeactivatableInterface
{
    const INSTANCE_TYPE_ESNAMANAGER_PRO = 1;

    use ReviewableDocumentTrait;

    /**
     * @ODM\Id
     * @Gedmo\Versioned
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="n")
     */
    private $name;

    /**
     * @ODM\Boolean(name="i")
     */
    private $inUse = false;

    /**
     * @ODM\Boolean(name="a")
     */
    private $active = true;


    public function __construct()
    {
        $this->id = new \MongoId();
        $this->setReviewStatus(InstanceTransitions::NONE);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return boolean
     */
    public function isInUse()
    {
        return $this->inUse;
    }

    /**
     * @param boolean $inUse
     */
    public function setInUse($inUse)
    {
        $this->inUse = $inUse;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}
