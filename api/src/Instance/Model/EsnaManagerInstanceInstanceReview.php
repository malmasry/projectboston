<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Model;

use JMS\Serializer\Annotation as Serializer;
use ProjectBoston\CustomerServiceCenter\Review\Model\ObjectReview;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class EsnaManagerInstanceInstanceReview extends ObjectReview
{
    /**
     * @var EsnaManagerInstance
     */
    protected $object;
}
