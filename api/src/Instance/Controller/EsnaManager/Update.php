<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Controller\EsnaManagerInstancePro;

use ProjectBoston\CustomerServiceCenter\Instance\InstanceTransitions;
use ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstanceInstanceReview;
use SM\Factory\FactoryInterface;
use SM\SMException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Update
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param EsnaManagerInstanceInstanceReview $esnaInstanceReview
     *
     * @throws SMException
     *
     * @return Response
     */
    public function __invoke(EsnaManagerInstanceInstanceReview $esnaInstanceReview)
    {
        $this->sm->get($esnaInstanceReview, InstanceTransitions::GRAPH)->apply(InstanceTransitions::CREATE_EDIT);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
