<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Controller\EsnaManagerInstancePro;

use ProjectBoston\CustomerServiceCenter\Instance\Model\Instance;
use ProjectBoston\CustomerServiceCenter\Instance\Repository\InstanceRepository;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class All
{
    private $instanceRepository;

    /**
     * @param InstanceRepository $instanceRepository
     */
    public function __construct(InstanceRepository $instanceRepository)
    {
        $this->instanceRepository = $instanceRepository;
    }

    /**
     * @param string $type
     *
     * @return Instance[]
     */
    public function __invoke($type)
    {
        if ($type === 'all') {
            return $this->instanceRepository->findAll();
        }

        return $this->instanceRepository->findActive();
    }
}
