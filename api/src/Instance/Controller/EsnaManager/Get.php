<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Controller\EsnaManagerInstancePro;

use ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstance;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Get
{
    /**
     * @param EsnaManagerInstance $instance
     *
     * @return EsnaManagerInstance
     */
    public function __invoke(EsnaManagerInstance $instance)
    {
        return $instance;
    }
}
