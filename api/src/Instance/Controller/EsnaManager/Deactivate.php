<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Controller\EsnaManagerInstancePro;

use ProjectBoston\CustomerServiceCenter\Instance\InstanceTransitions;
use ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstance;
use ProjectBoston\CustomerServiceCenter\Review\Model\Review;
use ProjectBoston\CustomerServiceCenter\Review\Model\ObjectReview;
use SM\Factory\FactoryInterface;
use SM\SMException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Deactivate
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param EsnaManagerInstance $instance
     * @param Review                  $review
     *
     * @throws AccessDeniedHttpException
     * @throws SMException
     *
     * @return Response
     */
    public function __invoke(EsnaManagerInstance $instance, Review $review)
    {
        if (!$instance->isInUse()) {
            throw new AccessDeniedHttpException('Product is not in use and not deactivatable, use delete instead.');
        }

        $this->sm->get(new ObjectReview($instance, $review), InstanceTransitions::GRAPH)->apply(
            InstanceTransitions::CREATE_DEACTIVATE
        );

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
