<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Controller\EsnaManagerInstancePro;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstance;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Delete
{
    private $dm;

    /**
     * @param ObjectManager $dm
     */
    public function __construct(ObjectManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param EsnaManagerInstance $instance
     *
     * @throws AccessDeniedHttpException
     *
     * @return Response
     */
    public function __invoke(EsnaManagerInstance $instance)
    {
        if ($instance->isInUse()) {
            throw new AccessDeniedHttpException('Product is in use and not deletable, use deactivate instead.');
        }

        $this->dm->remove($instance);
        $this->dm->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
