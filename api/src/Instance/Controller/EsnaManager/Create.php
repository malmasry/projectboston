<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Controller\EsnaManagerInstancePro;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstance;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Create
{
    private $dm;

    /**
     * @param ObjectManager $dm
     */
    public function __construct(ObjectManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param EsnaManagerInstance $instance
     *
     * @return Response
     */
    public function __invoke(EsnaManagerInstance $instance)
    {
        $this->dm->persist($instance);
        $this->dm->flush();

        return new Response('', Response::HTTP_CREATED);
    }
}
