<?php

namespace ProjectBoston\CustomerServiceCenter\Instance;

use ProjectBoston\CustomerServiceCenter\Core\Transitions;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class InstanceTransitions extends Transitions
{
    const GRAPH = 'csc_instance';
}
