<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use ProjectBoston\CustomerServiceCenter\Instance\Model\EsnaManagerInstance;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class InstanceDocumentRepository extends DocumentRepository implements InstanceRepository
{
    /**
     * @param mixed $id
     *
     * @return ReviewableInterface
     */
    public function findReviewableObjectById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @return EsnaManagerInstance[]
     */
    public function findActive()
    {
        return $this->findBy(['active' => true]);
    }
}
