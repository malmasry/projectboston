<?php

namespace ProjectBoston\CustomerServiceCenter\Instance\Repository;

use ProjectBoston\CustomerServiceCenter\Instance\Model\Instance;
use ProjectBoston\CustomerServiceCenter\Review\Repository\ReviewableRepository;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
interface InstanceRepository extends ReviewableRepository
{

    /**
     * @return Instance[]
     */
    public function findAll();

    /**
     * @return Instance[]
     */
    public function findActive();
}
