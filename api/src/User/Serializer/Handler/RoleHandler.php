<?php

namespace ProjectBoston\CustomerServiceCenter\User\Serializer\Handler;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class RoleHandler
{
    private $objectRoles;

    public function __construct(ArrayCollection $objectRoles)
    {
        $this->objectRoles = $objectRoles;
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param Role                     $role
     *
     * @return string
     */
    public function serializeRoleToJson(JsonSerializationVisitor $visitor, Role $role)
    {
        return $role->getName();
    }

    /**
     * @param JsonDeserializationVisitor $visitor
     * @param Role                       $role
     *
     * @return string
     */
    public function deserializeJsonToRole(JsonDeserializationVisitor $visitor, $role)
    {
        $findRole = function (Role $obj) use ($role) {
            return $obj->getName() === $role;
        };

        if ($roleObj = $this->objectRoles->filter($findRole)->first()) {
            return $roleObj;
        }

        return new Role($role, $role);
    }
}
