<?php

namespace ProjectBoston\CustomerServiceCenter\User\UserChecker;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Util\SecureRandomInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserChecker implements UserCheckerInterface
{
    private $dm;
    private $logger;
    private $hashGenerator;

    /**
     * @param ObjectManager         $dm
     * @param LoggerInterface       $logger
     * @param SecureRandomInterface $hashGenerator
     */
    public function __construct(ObjectManager $dm, LoggerInterface $logger, SecureRandomInterface $hashGenerator)
    {
        $this->hashGenerator = $hashGenerator;
        $this->logger        = $logger;
        $this->dm            = $dm;
    }

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return bool
     */
    public function checkUser($data, User $user = null)
    {
        if (null === $user) {
            $this->logger->error(sprintf('User with ID "%s" not found.', $data->id));

            return false;
        }

        if ($user->isEnabled()) {
            $this->logger->error(sprintf('User with ID "%s" was enabled.', $data->id));

            return false;
        }

        $user->setConfirmationToken(
            str_replace(['+', '/', '='], ['-', '_', ','], base64_encode($this->hashGenerator->nextBytes(32)))
        );
        $this->dm->flush();
        $this->dm->clear();

        return true;
    }
}
