<?php

namespace ProjectBoston\CustomerServiceCenter\User\UserChecker;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserCheckerChain implements UserCheckerInterface
{
    /**
     * @var UserCheckerInterface[]
     */
    private $userChecker = [];

    /**
     * @param int                  $type
     * @param UserCheckerInterface $userChecker
     */
    public function addUserChecker($type, UserCheckerInterface $userChecker)
    {
        $this->userChecker[$type] = $userChecker;
    }

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return bool
     */
    public function checkUser($data, User $user = null)
    {
        if (array_key_exists($data->type, $this->userChecker)) {
            return $this->userChecker[$data->type]->checkUser($data, $user);
        }

        return false;
    }
}
