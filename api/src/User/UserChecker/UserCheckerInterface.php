<?php

namespace ProjectBoston\CustomerServiceCenter\User\UserChecker;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
interface UserCheckerInterface
{
    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return bool
     */
    public function checkUser($data, User $user = null);
}
