<?php

namespace ProjectBoston\CustomerServiceCenter\User\UserChecker;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Util\SecureRandomInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UnlockedUserChecker implements UserCheckerInterface
{
    private $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return bool
     */
    public function checkUser($data, User $user = null)
    {
        if (null === $user) {
            $this->logger->error(sprintf('User with ID "%s" not found.', $data->id));

            return false;
        }

        if (!$user->isAccountNonLocked()) {
            $this->logger->error(sprintf('User with ID "%s" is locked.', $data->id));

            return false;
        }

        return true;
    }
}
