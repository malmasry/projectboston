<?php

namespace ProjectBoston\CustomerServiceCenter\User\Mailer;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
interface UserMailerInterface
{
    const NEW_USER_MAIL       = 1;
    const RESET_PASSWORD_MAIL = 2;
    const UNLOCKED_MAIL       = 3;

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return int
     */
    public function sendEmail(\stdClass $data, User $user);
}
