<?php

namespace ProjectBoston\CustomerServiceCenter\User\Mailer;

use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\TemplateParamGeneratorInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserMailer implements UserMailerInterface
{
    private $mailer;
    private $translator;
    private $templating;
    private $templateParamGenerator;
    private $config;

    /**
     * @param \Swift_Mailer                   $mailer
     * @param TranslatorInterface             $translator
     * @param EngineInterface                 $templating
     * @param TemplateParamGeneratorInterface $templateParamGenerator ,
     * @param array                           $config
     */
    public function __construct(
        \Swift_Mailer $mailer,
        TranslatorInterface $translator,
        EngineInterface $templating,
        TemplateParamGeneratorInterface $templateParamGenerator,
        array $config
    ) {
        $this->mailer                 = $mailer;
        $this->translator             = $translator;
        $this->templating             = $templating;
        $this->templateParamGenerator = $templateParamGenerator;
        $this->config                 = $config;
    }

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return int
     */
    public function sendEmail(\stdClass $data, User $user)
    {
        $this->translator->setLocale($user->getLocale());

        $txtBody = $this->templating->render(
            $this->config['txtTemplate'],
            $this->templateParamGenerator->getParameters($data, $user)
        );

        $htmlBody = $this->templating->render(
            $this->config['htmlTemplate'],
            $this->templateParamGenerator->getParameters($data, $user)
        );

        /** @var $message \Swift_Message */
        $message = $this->mailer->createMessage();
        $message->setSubject($this->translator->trans($this->config['subject'], [], 'mail'))
            ->setFrom($this->config['fromAddress'])
            ->setTo($user->getEmail())
            ->setBody($txtBody)
            ->addPart($htmlBody, 'text/html');

        if (!$this->mailer->getTransport()->isStarted()) {
            $this->mailer->getTransport()->start();
        }
        $result = $this->mailer->send($message);
        $this->mailer->getTransport()->stop();

        return $result;
    }
}
