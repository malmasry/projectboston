<?php

namespace ProjectBoston\CustomerServiceCenter\User\Mailer;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserMailerChain implements UserMailerInterface
{
    /**
     * @var UserMailerInterface[]
     */
    private $userMailer = [];

    /**
     * @param int                 $type
     * @param UserMailerInterface $userMailer
     */
    public function addUserMailer($type, UserMailerInterface $userMailer)
    {
        $this->userMailer[$type] = $userMailer;
    }

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return int
     */
    public function sendEmail(\stdClass $data, User $user)
    {
        if (array_key_exists($data->type, $this->userMailer)) {
            return $this->userMailer[$data->type]->sendEmail($data, $user);
        }

        return 0;
    }
}
