<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\Security;

use ProjectBoston\CustomerServiceCenter\User\Model\CreationUser;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use SM\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class RequestPassword
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param Request           $request
     * @param CreationUser|null $user
     *
     * @return Response
     */
    public function __invoke(Request $request, CreationUser $user = null)
    {
        if (isset($user) && $user->isAccountNonLocked()) {
            $user->setBaseUrl($request->request->get('baseUrl'));
            $this->sm->get($user, UserTransitions::GRAPH_SECURITY)->apply(UserTransitions::REQUEST_PASSWORD);
        }

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
