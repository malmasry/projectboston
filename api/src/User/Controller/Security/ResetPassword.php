<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\Security;

use ProjectBoston\CustomerServiceCenter\User\Model\SetPassword;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use SM\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class ResetPassword
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param User        $user
     * @param SetPassword $setPassword
     *
     * @return Response
     */
    public function __invoke(User $user, SetPassword $setPassword)
    {
        $user->setPassword($setPassword->getPlainPassword());
        $this->sm->get($user, UserTransitions::GRAPH_SECURITY)->apply(UserTransitions::SET_PASSWORD);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
