<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\Security;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class GetRoleStructure
{
    private $roleStructure;

    /**
     * @param $roleStructure array
     */
    public function __construct(array $roleStructure)
    {
        $this->roleStructure = $roleStructure;
    }

    /**
     * @return array
     */
    public function __invoke()
    {
        return $this->roleStructure;
    }
}
