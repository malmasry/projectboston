<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\Profile;

use ProjectBoston\CustomerServiceCenter\User\Model\SetProfilePassword;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use SM\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class SetPassword
{
    private $sm;
    private $tokenStorage;

    /**
     * @param FactoryInterface      $sm
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(FactoryInterface $sm, TokenStorageInterface $tokenStorage)
    {
        $this->sm = $sm;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param SetProfilePassword $setProfilePassword
     *
     * @return Response
     */
    public function __invoke(SetProfilePassword $setProfilePassword)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $user->setPassword($setProfilePassword->getPlainPassword());
        $this->sm->get($user, UserTransitions::GRAPH_SECURITY)->apply(UserTransitions::SET_PASSWORD);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
