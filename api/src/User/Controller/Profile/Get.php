<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\Profile;

use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Get
{
    private $tokenStorage;
    private $roleAliasHierarchy;

    /**
     * @param TokenStorageInterface  $tokenStorage
     * @param RoleHierarchyInterface $roleAliasHierarchy
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        RoleHierarchyInterface $roleAliasHierarchy
    ) {
        $this->tokenStorage       = $tokenStorage;
        $this->roleAliasHierarchy = $roleAliasHierarchy;
    }

    /**
     * @return User
     */
    public function __invoke()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $user->setReachableRoles($this->roleAliasHierarchy->getReachableRoles($user->getRoles()));

        return $user;
    }
}
