<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use ProjectBoston\CustomerServiceCenter\Review\Model\ObjectReview;
use ProjectBoston\CustomerServiceCenter\Review\Model\Review;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use SM\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Delete
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param User   $userInstance
     * @param Review $review
     *
     * @return Response
     */
    public function __invoke(User $userInstance, Review $review)
    {
        $this->sm->get(new ObjectReview($userInstance, $review), UserTransitions::GRAPH)
            ->apply(UserTransitions::CREATE_DELETE);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
