<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use ProjectBoston\CustomerServiceCenter\User\Model\CreationUser;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use SM\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Create
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param CreationUser $userInstance
     *
     * @return Response
     */
    public function __invoke(CreationUser $userInstance)
    {
        $this->sm->get($userInstance, UserTransitions::GRAPH_SECURITY)
            ->apply(UserTransitions::CREATE_USER);

        return new Response('', Response::HTTP_CREATED);
    }
}
