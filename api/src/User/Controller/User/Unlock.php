<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use ProjectBoston\CustomerServiceCenter\User\Model\CreationUser;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use SM\Factory\FactoryInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Unlock
{
    private $sm;

    /**
     * @param FactoryInterface $sm
     */
    public function __construct(FactoryInterface $sm)
    {
        $this->sm = $sm;
    }

    /**
     * @param User $userInstance
     *
     * @return Response
     */
    public function __invoke(User $userInstance)
    {
        $this->sm->get($userInstance, UserTransitions::GRAPH_SECURITY)
            ->apply(UserTransitions::UNLOCK_USER);

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
