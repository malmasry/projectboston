<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Lock
{
    private $dm;

    /**
     * @param ObjectManager $dm
     */
    public function __construct(ObjectManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param User $userInstance
     *
     * @return Response
     */
    public function __invoke(User $userInstance)
    {
        $userInstance->setLocked(true);
        $this->dm->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
