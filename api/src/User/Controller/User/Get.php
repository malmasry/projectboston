<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class Get
{
    /**
     * @param User $userInstance
     *
     * @return User
     */
    public function __invoke(User $userInstance)
    {
        return $userInstance;
    }
}
