<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\User\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class All
{
    private $userRepository;
    private $tokenStorage;

    /**
     * @param UserRepository        $userRepository
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        UserRepository $userRepository,
        TokenStorageInterface $tokenStorage
    ) {
        $this->userRepository = $userRepository;
        $this->tokenStorage   = $tokenStorage;
    }

    /**
     * @param int $customer
     *
     * @return array
     */
    public function __invoke($customer)
    {
        $customerId = $this->tokenStorage->getToken()->getUser()->getCustomer()->getId();
        if (null !== $customer) {
            $customerId = (int)$customer;
        }

        if (null === $customer &&
            $this->tokenStorage->getToken()->getUser()->getCustomer()->getType() === Customer::TYPE_ADMIN
        ) {
            return $this->userRepository->findAllUsers();
        }

        return $this->userRepository->findAllUsersWithCustomer($customerId);
    }
}
