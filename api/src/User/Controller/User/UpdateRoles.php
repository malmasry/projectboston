<?php

namespace ProjectBoston\CustomerServiceCenter\User\Controller\User;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class UpdateRoles
{
    private $dm;

    /**
     * @param ObjectManager $dm
     */
    public function __construct(ObjectManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @return Response
     */
    public function __invoke()
    {
        $this->dm->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
