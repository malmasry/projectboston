<?php

namespace ProjectBoston\CustomerServiceCenter\User\Repository;

use ProjectBoston\CustomerServiceCenter\Core\Repository\DeserializableRepository;
use ProjectBoston\CustomerServiceCenter\Review\Repository\ReviewableRepository;
use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
interface UserRepository extends ReviewableRepository, DeserializableRepository
{
    /**
     * @param string $id
     *
     * @return User
     */
    public function findUserById($id);

    /**
     * @param int $id
     *
     * @return User[]
     */
    public function findAllUsersWithCustomer($id);

    /**
     * @return User[]
     */
    public function findAllUsers();

    /**
     * @param array $customerIds
     *
     * @return User[]
     */
    public function findAllUsersByCustomerIds($customerIds);

    /**
     * @param int $customerId
     * @param array $excludedUserIds
     * @param array $roles
     *
     * @return User[]
     */
    public function findAllReviewer($customerId, $excludedUserIds, $roles);

    /**
     * @param string $token
     *
     * @return User
     */
    public function findByConfirmationToken($token);
}
