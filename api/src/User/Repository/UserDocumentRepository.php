<?php

namespace ProjectBoston\CustomerServiceCenter\User\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableInterface;
use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class UserDocumentRepository extends DocumentRepository implements UserRepository
{
    /**
     * @param string $id
     *
     * @return User
     */
    public function findUserById($id)
    {
        $this->dm->clear();
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param int $id
     *
     * @return User[]
     */
    public function findAllUsersWithCustomer($id)
    {
        return $this->findBy(['customer.id' => $id], ['email' => 'ASC']);
    }

    /**
     * @return User[]
     */
    public function findAllUsers()
    {
        return $this->findBy([], ['email' => 'ASC']);
    }

    /**
     * @param string $token
     *
     * @return User
     */
    public function findByConfirmationToken($token)
    {
        return $this->findOneBy(['confirmationToken' => $token]);
    }

    /**
     * @param mixed $id
     *
     * @return ReviewableInterface
     */
    public function findReviewableObjectById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param array $customerIds
     *
     * @return User[]
     */
    public function findAllUsersByCustomerIds($customerIds)
    {
        $qb = $this->createQueryBuilder();
        return $qb
            ->find()
            ->field('customer.id')->in($customerIds)
            ->getQuery()
            ->execute()
            ->toArray(false);
    }

    /**
     * @param int $customerId
     * @param array $excludedUserIds
     * @param array $roles
     *
     * @return User[]
     */
    public function findAllReviewer($customerId, $excludedUserIds, $roles)
    {
        $qb = $this->createQueryBuilder();
        return $qb
            ->find()
            ->addAnd($qb->expr()->field('id')->notIn($excludedUserIds))
            ->addAnd($qb->expr()->field('customer.id')->equals($customerId))
            ->addAnd($qb->expr()->field('reviewStatus')->equals('none'))
            ->addAnd($qb->expr()->field('enabled')->equals(true))
            ->addAnd($qb->expr()->field('locked')->equals(false))
            ->addAnd($qb->expr()->field('roles.role')->in($roles))
            ->getQuery()
            ->execute()
            ->toArray(false);
    }

    /**
     * @param string $id
     *
     * @return User
     */
    public function findOneById($id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    /**
     * @param string[] $ids
     *
     * @return User[]
     */
    public function findByIds(array $ids)
    {
        $qb = $this->createQueryBuilder();
        return $qb
            ->find()
            ->field('id')->in($ids)
            ->getQuery()
            ->execute()
            ->toArray(false);
    }
}
