<?php

namespace ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class TemplateParamGeneratorChain implements TemplateParamGeneratorInterface
{
    /**
     * @var TemplateParamGeneratorInterface[]
     */
    private $paramGenerators = [];

    /**
     * @param int                             $type
     * @param TemplateParamGeneratorInterface $templateParamGenerator
     */
    public function addParamGenerator($type, TemplateParamGeneratorInterface $templateParamGenerator)
    {
        $this->paramGenerators[$type] = $templateParamGenerator;
    }

    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return array
     */
    public function getParameters(\stdClass $data, User $user)
    {
        if (array_key_exists($data->type, $this->paramGenerators)) {
            return $this->paramGenerators[$data->type]->getParameters($data, $user);
        }

        return [];
    }
}
