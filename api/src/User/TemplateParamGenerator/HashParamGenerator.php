<?php

namespace ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator;

use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class HashParamGenerator implements TemplateParamGeneratorInterface
{
    /**
     * @param \stdClass $data
     * @param User      $user
     *
     * @return array
     */
    public function getParameters(\stdClass $data, User $user)
    {
        return ['baseUrl' => $data->baseUrl, 'hash' => $user->getConfirmationToken()];
    }
}
