<?php

namespace ProjectBoston\CustomerServiceCenter\User\DependencyInjection\Security\UserProvider;

use Symfony\Bridge\Doctrine\DependencyInjection\Security\UserProvider\EntityFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class CanonicalizedDocumentFactory extends EntityFactory
{
    public function create(ContainerBuilder $container, $id, $config)
    {
        parent::create($container, $id, $config);

        $userProviderDefinition = $container->getDefinition($id);
        $arguments = array_reverse($userProviderDefinition->getArguments());
        $arguments[] = new Reference('projectboston.csc.user.canonicalizer');

        $userProviderDefinition->setArguments(array_reverse($arguments));
    }
}
