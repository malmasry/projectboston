<?php

namespace ProjectBoston\CustomerServiceCenter\User\DependencyInjection;

use Matthias\BundlePlugins\SimpleBundlePlugin;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserPlugin extends SimpleBundlePlugin
{
    /**
     * @inheritdoc
     */
    public function name()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function addConfiguration(ArrayNodeDefinition $pluginNode)
    {
        $pluginNode
            ->children()
                ->scalarNode('from_address')->cannotBeEmpty()->isRequired()->end()
                ->arrayNode('roles')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->prototype('array') // role group
                        ->isRequired()
                        ->requiresAtLeastOneElement()
                        ->useAttributeAsKey('alias')
                        ->prototype('array') // concrete role
                            ->children()
                                ->scalarNode('role')->isRequired()->end() //symfony role name
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }


    /**
     * @inheritdoc
     */
    public function load(array $pluginConfiguration, ContainerBuilder $container)
    {
        $container->setParameter('project_boston_csc.user.from_address', $pluginConfiguration['from_address']);

        $aliasStructure = [];
        foreach ($pluginConfiguration['roles'] as $roleGroupName => $roleGroup) {
            foreach (array_keys($roleGroup) as $alias) {
                $aliasStructure[$roleGroupName][] = $alias;
            }
        }

        $container->setParameter('project_boston_csc.user.alias_roles', $aliasStructure);

        $roleObjects = $container->getDefinition('projectboston.csc.user.object_roles');
        foreach ($pluginConfiguration['roles'] as $roleGroup) {
            foreach ($roleGroup as $alias => $name) {
                $roleObjects->addMethodCall('add', [new Definition(Role::class, [$name['role'], $alias])]);
            }
        }
    }
}
