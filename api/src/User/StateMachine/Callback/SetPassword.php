<?php

namespace ProjectBoston\CustomerServiceCenter\User\StateMachine\Callback;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class SetPassword
{
    private $passwordEncoder;
    private $dm;

    /**
     * @param PasswordEncoderInterface $passwordEncoder
     * @param ObjectManager            $dm
     *
     */
    public function __construct(PasswordEncoderInterface $passwordEncoder, ObjectManager $dm)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->dm              = $dm;
    }

    /**
     * @param User $user
     */
    public function execute(User $user)
    {
        $user->setConfirmationToken(null);
        $user->setPassword($this->passwordEncoder->encodePassword($user->getPassword(), null));
        $user->setEnabled(true);
        $this->dm->flush();
    }
}
