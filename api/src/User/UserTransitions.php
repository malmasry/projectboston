<?php

namespace ProjectBoston\CustomerServiceCenter\User;

use ProjectBoston\CustomerServiceCenter\Core\Transitions;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class UserTransitions extends Transitions
{
    const GRAPH = 'csc_user';
    const GRAPH_SECURITY = 'csc_user_security';

    const SET_PASSWORD = 'set_password';
    const REQUEST_PASSWORD = 'request_password';
    const CREATE_USER = 'create_user';
    const UNLOCK_USER = 'unlock_user';
}
