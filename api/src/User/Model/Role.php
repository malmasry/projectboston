<?php

namespace ProjectBoston\CustomerServiceCenter\User\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @ODM\EmbeddedDocument
 *
 * @Gedmo\Loggable()
 *
 * @codeCoverageIgnore
 */
class Role implements RoleInterface
{
    /**
     * @Gedmo\Versioned
     * @ODM\String(name="n")
     */
    private $name;

    /**
     * @Gedmo\Versioned
     * @ODM\String(name="r")
     */
    private $role;

    /**
     * @param string $role
     * @param string $name
     */
    public function __construct($role, $name)
    {
        $this->role = $role;
        $this->name = $name;
    }

    /**
     * @param string $role
     *
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Returns the role.
     *
     * This method returns a string representation whenever possible.
     *
     * @return string A string representation of the role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
