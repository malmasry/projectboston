<?php

namespace ProjectBoston\CustomerServiceCenter\User\Model;

use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class SetProfilePassword extends SetPassword
{
    /**
     * @var string
     * @UserPassword()
     */
    private $current;
}
