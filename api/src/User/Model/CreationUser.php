<?php

namespace ProjectBoston\CustomerServiceCenter\User\Model;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @ODM\Document(
 *  collection="user",
 *  repositoryClass="ProjectBoston\CustomerServiceCenter\User\Repository\UserDocumentRepository"
 * )
 *
 * @Gedmo\Loggable(logEntryClass="ProjectBoston\CustomerServiceCenter\Core\Model\LogEntry")
 *
 * @codeCoverageIgnore
 */
class CreationUser extends User
{
    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @param string $baseUrl
     *
     * @return CreationUser
     */
    public function setBaseUrl($baseUrl)
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }
}
