<?php

namespace ProjectBoston\CustomerServiceCenter\User\Model;

use Doctrine\Common\Collections\Collection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Gedmo\Mapping\Annotation as Gedmo;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableDocumentTrait;
use ProjectBoston\CustomerServiceCenter\Review\Model\ReviewableInterface;
use ProjectBoston\CustomerServiceCenter\User\UserTransitions;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @ODM\Document(
 *  collection="user",
 *  repositoryClass="ProjectBoston\CustomerServiceCenter\User\Repository\UserDocumentRepository"
 * )
 *
 * @Gedmo\Loggable(logEntryClass="ProjectBoston\CustomerServiceCenter\Core\Model\LogEntry")
 *
 * @codeCoverageIgnore
 */
class User implements AdvancedUserInterface, \Serializable, ReviewableInterface
{
    use ReviewableDocumentTrait;

    /**
     * @ODM\Id
     */
    private $id;

    /**
     * @ODM\String(name="fn")
     * @Gedmo\Versioned
     */
    private $firstName;

    /**
     * @ODM\String(name="ln")
     * @Gedmo\Versioned
     */
    private $lastName;

    /**
     * @ODM\String(name="pw")
     */
    private $password;

    /**
     * @ODM\String(name="e")
     * @Gedmo\Versioned
     */
    private $email;

    /**
     * @ODM\String(name="ec")
     * @ODM\UniqueIndex
     * @Gedmo\Versioned
     */
    private $emailCanonical;


    /**
     * @ODM\Boolean(name="en")
     */
    private $enabled;

    /**
     * @ODM\String(name="ct")
     */
    private $confirmationToken;

    /**
     * @ODM\String(name="l")
     * @Gedmo\Versioned
     */
    private $locale;

    /**
     * @ODM\ReferenceOne(targetDocument="ProjectBoston\CustomerServiceCenter\Customer\Model\Customer", name="c")
     * @Gedmo\Versioned
     */
    private $customer;

    /**
     * @ODM\EmbedMany(targetDocument="ProjectBoston\CustomerServiceCenter\User\Model\Role", name="r")
     * @Gedmo\Versioned
     */
    private $roles;

    /**
     * @ODM\Boolean(name="lo")
     */
    private $locked;
    private $reachableRoles = [];
    private $reviewCount;

    public function __construct()
    {
        $this->enabled = false;
        $this->locked = false;
        $this->roles = [new Role('ROLE_USER', 'user')];
        $this->setReviewStatus(UserTransitions::NONE);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @param boolean $active
     *
     * @return User
     */
    public function setEnabled($active)
    {
        $this->enabled = $active;

        return $this;
    }

    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return Boolean true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->enabled;
    }


    /**
     * @param string $confirmationToken
     *
     * @return User
     */
    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    /**
     * @return string
     */
    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * @param string $locale
     *
     * @return User
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param \ProjectBoston\CustomerServiceCenter\Customer\Model\Customer $customer
     *
     * @return User
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return \ProjectBoston\CustomerServiceCenter\Customer\Model\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        if ($this->roles instanceof Collection) {
            return $this->roles->toArray();
        }

        return $this->roles;
    }

    /**
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {

    }

    /**
     * @param RoleInterface[] $reachableRoles
     *
     * @return User
     */
    public function setReachableRoles(array $reachableRoles)
    {
        $this->reachableRoles = $reachableRoles;

        return $this;
    }

    /**
     * @return array
     */
    public function getReachableRoles()
    {
        return $this->reachableRoles;
    }

    /**
     * @return string
     */
    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * @param string $emailCanoical
     *
     * @return User
     */
    public function setEmailCanonical($emailCanoical)
    {
        $this->emailCanonical = $emailCanoical;

        return $this;
    }

    /**
     * @param int $reviewCount
     *
     * @return User
     */
    public function setReviewCount($reviewCount)
    {
        $this->reviewCount = $reviewCount;

        return $this;
    }

    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return Boolean true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }

    /**
     * @param boolean $locked
     *
     * @return User
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return Boolean true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return Boolean true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     *
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(
            [
                $this->email,
                $this->password,
                $this->id,
                $this->emailCanonical
            ]
        );
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     *
     * @link http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     *
     * @return void
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        list (
            $this->email,
            $this->password,
            $this->id,
            $this->emailCanonical
            ) = $data;
    }
}
