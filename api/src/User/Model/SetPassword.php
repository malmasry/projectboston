<?php

namespace ProjectBoston\CustomerServiceCenter\User\Model;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class SetPassword
{
    private $plainPassword;
    private $repeatedPlainPassword;

    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @return string
     */
    public function getRepeatedPlainPassword()
    {
        return $this->repeatedPlainPassword;
    }
}
