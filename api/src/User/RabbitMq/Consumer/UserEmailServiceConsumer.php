<?php

namespace ProjectBoston\CustomerServiceCenter\User\RabbitMq\Consumer;

use ProjectBoston\CustomerServiceCenter\Core\RabbitMq\Consumer\ServiceConsumerInterface;
use ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailerInterface;
use ProjectBoston\CustomerServiceCenter\User\Repository\UserRepository;
use ProjectBoston\CustomerServiceCenter\User\UserChecker\UserCheckerInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserEmailServiceConsumer implements ServiceConsumerInterface
{
    private $userChecker;
    private $userRepository;
    private $userMailer;

    /**
     * @param UserRepository       $userRepository
     * @param UserCheckerInterface $userChecker
     * @param UserMailerInterface  $userMailer
     */
    public function __construct(
        UserRepository $userRepository,
        UserCheckerInterface $userChecker,
        UserMailerInterface $userMailer
    ) {
        $this->userRepository = $userRepository;
        $this->userChecker    = $userChecker;
        $this->userMailer     = $userMailer;
    }

    /**
     * @param \stdClass $data
     *
     * @return int|null
     */
    public function process($data)
    {
        $user = $this->userRepository->findUserById(new \MongoId($data->id));

        if (!$this->userChecker->checkUser($data, $user)) {
            return self::MSG_REJECT;
        }

        if ($this->userMailer->sendEmail($data, $user) > 0) {
            return self::MSG_ACK;
        }

        return self::MSG_SINGLE_NACK_REQUEUE;
    }
}
