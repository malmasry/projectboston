<?php

namespace ProjectBoston\CustomerServiceCenter\User\Validator\Constraints;

use Doctrine\Common\Collections\ArrayCollection;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class ValidRoleValidator extends ConstraintValidator
{
    private $objectRoles;

    public function __construct(ArrayCollection $objectRoles)
    {
        $this->objectRoles = $objectRoles;
    }

    /**
     * Checks if the role is valid
     *
     * @param Role $value      The value that should be validated
     * @param Constraint   $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        $checkIfRoleExists = function (Role $role) use ($value) {
            return $role->getName() === $value->getName();
        };

        if ($this->objectRoles->filter($checkIfRoleExists)->isEmpty()) {
            $this->context->addViolation(
                $constraint->message,
                ['{{ role }}' => $value->getName()],
                $value->getName()
            );
        }
    }
}
