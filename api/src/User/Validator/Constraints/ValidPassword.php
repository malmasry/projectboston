<?php

namespace ProjectBoston\CustomerServiceCenter\User\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint for the password validator
 *
 * @author Mohamed Almasry <almasry@almasry.ws>
 * @codeCoverageIgnore
 */
class ValidPassword extends Constraint
{
    public $message = 'The password "{{ plainPassword }}" is not equal with confirmation.';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
