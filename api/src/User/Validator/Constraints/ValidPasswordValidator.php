<?php

namespace ProjectBoston\CustomerServiceCenter\User\Validator\Constraints;

use ProjectBoston\CustomerServiceCenter\User\Model\SetPassword;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class ValidPasswordValidator extends ConstraintValidator
{
    /**
     * Checks if the password is valid
     *
     * @param SetPassword $value      The value that should be validated
     * @param Constraint  $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        if ($value->getPlainPassword() !== $value->getRepeatedPlainPassword()) {
            $this->context->addViolation(
                $constraint->message,
                ['{{ plainPassword }}' => $value->getPlainPassword()],
                $value->getPlainPassword()
            );
        }
    }
}
