<?php

namespace ProjectBoston\CustomerServiceCenter\User\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint for the role validator
 *
 * @author Mohamed Almasry <almasry@almasry.ws>
 * @codeCoverageIgnore
 */
class ValidRole extends Constraint
{
    public $message = 'The role "{{ role }}" does not exist.';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return string
     */
    public function validatedBy()
    {
        return 'role';
    }
}
