<?php

namespace ProjectBoston\CustomerServiceCenter\User\EventListener;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class SetPasswordViolationsListener
{
    private $dm;

    /**
     * @param ObjectManager $dm
     */
    public function __construct(ObjectManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * @param FilterControllerEvent $event
     */
    public function __invoke(FilterControllerEvent $event)
    {
        $request = $event->getRequest();

        $violations = $request->attributes->get('violations');
        $route      = $request->attributes->get('_route');
        $user       = $request->attributes->get('user');
        if ($route === 'project_boston_csc_user_security_reset_password' && $user instanceof User
            && $violations instanceof ConstraintViolationListInterface && $violations->count() > 0
        ) {
            $user->setConfirmationToken(null);
            $this->dm->flush();
        }
    }
}
