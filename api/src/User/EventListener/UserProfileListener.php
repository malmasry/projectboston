<?php

namespace ProjectBoston\CustomerServiceCenter\User\EventListener;

use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use ProjectBoston\CustomerServiceCenter\Review\Repository\ReviewRepository;
use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class UserProfileListener
{
    private $reviewRepository;

    public function __construct(ReviewRepository $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @param PreSerializeEvent $event
     */
    public function preSerialize(PreSerializeEvent $event)
    {
        /** @var User $obj */
        $obj = $event->getObject();
        $event->getContext()->attributes->get('groups')->map(
            function (array $groups) use ($obj) {
                if (in_array('profile', $groups, true)) {
                    $obj->setReviewCount($this->reviewRepository->countReviews($obj->getId()));
                }
            }
        );
    }
}
