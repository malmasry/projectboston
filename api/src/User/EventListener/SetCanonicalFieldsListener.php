<?php

namespace ProjectBoston\CustomerServiceCenter\User\EventListener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\Canonicalizer;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class SetCanonicalFieldsListener
{
    private $canonicalizer;

    /**
     * @param Canonicalizer $canonicalizer
     */
    public function __construct(Canonicalizer $canonicalizer)
    {
        $this->canonicalizer = $canonicalizer;
    }

    /**
     * @param LifecycleEventArgs $event
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $obj = $event->getDocument();
        if ($obj instanceof User) {
            $obj->setEmailCanonical($this->canonicalizer->canonicalize($obj->getEmail()));
        }
    }
}
