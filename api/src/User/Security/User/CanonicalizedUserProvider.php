<?php

namespace ProjectBoston\CustomerServiceCenter\User\Security\User;

use Doctrine\Common\Persistence\ManagerRegistry;
use ProjectBoston\CustomerServiceCenter\User\Canonicalizer;
use Symfony\Bridge\Doctrine\Security\User\EntityUserProvider;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @codeCoverageIgnore
 */
class CanonicalizedUserProvider extends EntityUserProvider
{
    private $canonicalizer;

    /**
     * @param ManagerRegistry $registry
     * @param Canonicalizer   $canonicalizer
     * @param null            $class
     * @param null            $property
     * @param null            $managerName
     */
    public function __construct(
        ManagerRegistry $registry,
        Canonicalizer $canonicalizer,
        $class,
        $property = null,
        $managerName = null
    ) {
        parent::__construct($registry, $class, $property, $managerName);

        $this->canonicalizer = $canonicalizer;
    }

    /**
     * @param string $username
     *
     * @return object|\Symfony\Component\Security\Core\User\UserInterface
     */
    public function loadUserByUsername($username)
    {
        return parent::loadUserByUsername($this->canonicalizer->canonicalize($username));
    }


}
