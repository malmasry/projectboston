<?php

namespace ProjectBoston\CustomerServiceCenter\User\Security\Role;

use Doctrine\Common\Collections\ArrayCollection;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class RoleAliasHierarchy implements RoleHierarchyInterface
{
    private $roleHierarchy;
    private $roleObjects;
    private $hierarchy;
    /**
     * @var array
     */
    private $map;

    /**
     * @param RoleHierarchyInterface $roleHierarchy
     * @param ArrayCollection        $roleObjects
     * @param array                  $hierarchy
     */
    public function __construct(RoleHierarchyInterface $roleHierarchy, ArrayCollection $roleObjects, array $hierarchy)
    {
        $this->roleHierarchy = $roleHierarchy;
        $this->roleObjects   = $roleObjects;
        $this->hierarchy     = $hierarchy;
        $this->buildRoleMap();
    }

    /**
     * Returns an array of all reachable roles by the given ones.
     *
     * Reachable roles are the roles directly assigned but also all roles that
     * are transitively reachable from them in the role hierarchy.
     *
     * @param RoleInterface[] $roles An array of directly assigned roles
     *
     * @return RoleInterface[] An array of all reachable roles
     */
    public function getReachableRoles(array $roles)
    {
        $reachableRoles = $this->roleHierarchy->getReachableRoles($roles);

        $roleCheck = function (RoleInterface $r1, RoleInterface $r2) {
            if ($r1->getRole() === $r2->getRole()) {
                return 0;
            }

            return $r1->getRole() > $r2->getRole() ? 1 : -1;
        };

        $reachableRolesAlias = [];
        /** @var $role Role */
        foreach (array_uintersect($this->roleObjects->toArray(), $reachableRoles, $roleCheck) as $role) {
            $reachableRolesAlias[] = $role->getName();
        }

        return $reachableRolesAlias;
    }

    public function getRoleHierarchyByAlias($alias)
    {

        $findRole = function (Role $obj) use ($alias) {
            return $obj->getName() === $alias;
        };

        /** @var Role $roleObj */
        if ($roleObj = $this->roleObjects->filter($findRole)->first()) {

            $hiera[] = $roleObj->getRole();

            foreach ($this->map as $main => $roles) {
                foreach ($roles as $role) {
                    if (in_array($role, $hiera, true)) {
                        $hiera[] = $main;
                        break;
                    }
                }
            }

            return $hiera;
        }

        return [];
    }

    private function buildRoleMap()
    {
        $this->map = array();
        foreach ($this->hierarchy as $main => $roles) {
            $this->map[$main] = $roles;
            $visited          = array();
            $additionalRoles  = $roles;
            while ($role = array_shift($additionalRoles)) {
                if (!isset($this->hierarchy[$role])) {
                    continue;
                }

                $visited[]        = $role;
                $this->map[$main] = array_unique(array_merge($this->map[$main], $this->hierarchy[$role]));
                $additionalRoles  = array_merge($additionalRoles, array_diff($this->hierarchy[$role], $visited));
            }
        }
    }
}
