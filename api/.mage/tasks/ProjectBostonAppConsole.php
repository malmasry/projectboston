<?php

namespace Task;

use Mage\Task\BuiltIn\Symfony2\SymfonyAbstractTask;

/**
 * Task for bin/console calls
 *
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class ProjectBostonAppConsole extends SymfonyAbstractTask
{
    public function getName()
    {
        return 'ProjectBoston Symfony v2 - bin/console';
    }

    public function run()
    {
        // Options
        $env = $this->getParameter('env', 'prod');
        $commandString = $this->getParameter('command', 'help');
        $command = $this->getAppPath() . ' ' . $commandString. ' --env=' . $env;
        $result = $this->runCommand($command);

        return $result;
    }
}
