<?php

namespace ProjectBoston\CustomerServiceCenter\Application;

use Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle;
use Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle;
use FOS\RestBundle\FOSRestBundle;
use Gfreeau\Bundle\GetJWTBundle\GfreeauGetJWTBundle;
use h4cc\AliceFixturesBundle\h4ccAliceFixturesBundle;
use JMS\SerializerBundle\JMSSerializerBundle;
use Knp\Bundle\GaufretteBundle\KnpGaufretteBundle;
use Lexik\Bundle\JWTAuthenticationBundle\LexikJWTAuthenticationBundle;
use ProjectBoston\Bundle\PuliMetadataFileLocatorBundle\MiPuliMetadataFileLocatorBundle;
use ProjectBoston\Bundle\RestExtraBundle\MiRestExtraBundle;
use ProjectBoston\Bundle\CustomerServiceCenter as ProjectBostonBundle;
use Misd\PhoneNumberBundle\MisdPhoneNumberBundle;
use Nelmio\ApiDocBundle\NelmioApiDocBundle;
use Nelmio\CorsBundle\NelmioCorsBundle;
use OldSound\RabbitMqBundle\OldSoundRabbitMqBundle;
use Puli\SymfonyBundle\PuliBundle;
use Sensio\Bundle\DistributionBundle\SensioDistributionBundle;
use Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle;
use Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle;
use Sixdays\OpcacheBundle\SixdaysOpcacheBundle;
use Stof\EsnaDoctrineExtensionBundle\StofEsnaDoctrineExtensionBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Bundle\WebProfilerBundle\WebProfilerBundle;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use winzou\Bundle\StateMachineBundle\winzouStateMachineBundle;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $class = PULI_FACTORY_CLASS;
        /** @var \Puli\GeneratedPuliFactory $repoFactory */
        $repoFactory = new $class;

        $repo = $repoFactory->createRepository();
        $discovery = $repoFactory->createDiscovery($repo);

        $bundles = [
            new FrameworkBundle(),
            new SecurityBundle(),
            new TwigBundle(),
            new MonologBundle(),
            new SwiftmailerBundle(),
            new SensioFrameworkExtraBundle(),
            new JMSSerializerBundle(),
            new FOSRestBundle(),
            new NelmioApiDocBundle(),
            new NelmioCorsBundle(),
            new DoctrineMongoDBBundle(),
            new OldSoundRabbitMqBundle(),
            new MisdPhoneNumberBundle(),
            new StofEsnaDoctrineExtensionBundle(),
            new KnpGaufretteBundle(),
            new LexikJWTAuthenticationBundle(),
            new GfreeauGetJWTBundle(),
            new winzouStateMachineBundle(),
            new PuliBundle(),
            new DoctrineCacheBundle(),
            new MiPuliMetadataFileLocatorBundle(),
            new ProjectBostonBundle\MainBundle\ProjectBostonCustomerServiceCenterMainBundle($discovery, $repo),
            new ProjectBostonBundle\AssignmentBundle\ProjectBostonCustomerServiceCenterAssignmentBundle(),
            new SixdaysOpcacheBundle(),
            new MiRestExtraBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new WebProfilerBundle();
            $bundles[] = new SensioDistributionBundle();
            $bundles[] = new SensioGeneratorBundle();
            $bundles[] = new h4ccAliceFixturesBundle();
        }

        if ($this->getEnvironment() === 'test') {
            $bundles[] = new ProjectBostonBundle\RabbitMQSynchronizerBundle\ProjectBostonCustomerServiceCenterRabbitMQSynchronizerBundle(
            );
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__ . '/config/config_' . $this->getEnvironment() . '.yml');
    }
}
