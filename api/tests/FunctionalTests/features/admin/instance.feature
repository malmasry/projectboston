@instance
Feature: API Calls for Instance as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  # READ
  @instance-list 
  Scenario: Expect a list of active esnainstances
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/esnainstance"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 6 elements
    And the JSON node "root[0].name" should exist
    And the JSON node "root[0].baseUrl" should exist
    And the JSON node "root[0].authUrl" should exist
    And the JSON node "root[0].authClientId" should exist
    And the JSON node "root[0].authClientSecret" should exist
    And the JSON node "root[0].authUsername" should exist
    And the JSON node "root[0].authPassword" should exist

  @instance-list @keep-database
  Scenario: Expect a list of all esnainstances
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/esnainstance?type=all"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 7 elements
    And the JSON node "root[0].name" should exist
    And the JSON node "root[0].baseUrl" should exist
    And the JSON node "root[0].authUrl" should exist
    And the JSON node "root[0].authClientId" should exist
    And the JSON node "root[0].authClientSecret" should exist
    And the JSON node "root[0].authUsername" should exist
    And the JSON node "root[0].authPassword" should exist


  @instance-single @keep-database
  Scenario: Expect an existing esnainstance
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[0].id" as placeholder "ESNA_ID1"
    When I send a GET request to "/api/esnainstance/ESNA_ID1" with placeholder
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.id" should exist
    And the JSON node "root.name" should exist
    And the JSON node "root.baseUrl" should exist
    And the JSON node "root.authUrl" should exist
    And the JSON node "root.authClientId" should exist
    And the JSON node "root.authClientSecret" should exist
    And the JSON node "root.authUsername" should exist
    And the JSON node "root.authPassword" should exist

  @instance-create  @keep-database
  Scenario: Try to create a invalid esnainstances
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/esnainstance" with placeholder and body:
    """
    {
      "name": "",
      "baseUrl": "",
      "authUrl": "",
      "authClientId": "",
      "authClientSecret": "",
      "authUsername": "",
      "authPassword": ""
    }
    """
    Then the response status code should be 400
    And the JSON node "root" should have 7 elements


  # EDIT
  @instance-edit @keep-database
  Scenario Outline: Try to edit an existing esna instance without review
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    When I send a PUT request to "/api/esnainstance/ID" with placeholder and body:
    """
    {
      "id" : "ID",
      "name": "ESNA default EDIT",
      "baseUrl": "https://esna.base.url.edit",
      "authUrl": "https://esna.auth.url.edit"
    }
    """
    Then the response status code should be 403
  Examples:
    | role            |
    | instance_create |
    | instance_delete |

  @instance-edit @mailer @keep-database
  Scenario Outline: Edit an existing esna instance and deny the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header

    And I send a GET request to "/api/reviewers?action=instance_edit"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/esnainstance/ID" with placeholder and body:
    """
    {
      "esnaInstance" : {
        "id" : "ID",
        "name": "ESNA default EDIT",
        "baseUrl": "https://esna.base.url.edit",
        "authUrl": "https://esna.auth.url.edit",
        "authClientId": "CSC-EDIT",
        "authClientSecret": "ThisIsTheSecret-Edit",
        "authUsername": "csc@projectboston.com.edit",
        "authPassword": "the-password-edit"
      },
      "review" : {
        "reviewer" : [
          "USER_ID1",
          "USER_ID2"
        ],
        "baseUrl": "http://csc.projectboston.dev/review/"
      }
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/esnainstance/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.name" should not contain "ESNA default EDIT"
    And the JSON node "root.baseUrl" should not contain "https://esna.base.url.edit"
    And the JSON node "root.authUrl" should not contain "https://esna.auth.url.edit"
    And the JSON node "root.authClientId" should not contain "CSC-EDIT"
    And the JSON node "root.authClientSecret" should not contain "ThisIsTheSecret-Edit"
    And the JSON node "root.authUsername" should not contain "csc@projectboston.com.edit"
    And the JSON node "root.authPassword" should not contain "the-password-edit"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/deny" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/esnainstance/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.name" should not contain "ESNA default EDIT"
    And the JSON node "root.baseUrl" should not contain "https://esna.base.url.edit"
    And the JSON node "root.authUrl" should not contain "https://esna.auth.url.edit"
    And the JSON node "root.authClientId" should not contain "CSC-EDIT"
    And the JSON node "root.authClientSecret" should not contain "ThisIsTheSecret-Edit"
    And the JSON node "root.authUsername" should not contain "csc@projectboston.com.edit"
    And the JSON node "root.authPassword" should not contain "the-password-edit"
    And the JSON node "root.reviewStatus" should contain "none"
    Examples:
      | role           |
      | instance_edit  |
      | instance_admin |
      | admin          |

  @instance-delete @keep-database
  Scenario Outline: Try to delete a esnainstance which is used
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[2].id" as placeholder "ID"
    And I send a DELETE request to "/api/esnainstance/ID" with placeholder
    Then the response status code should be 403
    Examples:
      | role            |
      | instance_create |
      | instance_edit   |
      | instance_delete |
      | instance_admin  |
      | admin           |

  # CREATE
  @instance-create
  Scenario Outline: Create a new esnainstances
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/esnainstance" with placeholder and body:
    """
    {
      "name": "ESNA default",
      "baseUrl": "https://esna.base.url",
      "authUrl": "https://esna.auth.url",
      "authClientId": "CSC",
      "authClientSecret": "ThisIsTheSecret",
      "authUsername": "csc@projectboston.com",
      "authPassword": "the-password"
    }
    """
    Then the response status code should be <code>
    Examples:
      | role    | code |
      | admin   | 201  |
      | partner | 403  |
      | user    | 403  |

  @instance-edit @mailer 
  Scenario Outline: Edit an existing esna instance and allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header

    And I send a GET request to "/api/reviewers?action=instance_edit"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/esnainstance/ID" with placeholder and body:
    """
    {
      "esnaInstance" : {
        "id" : "ID",
        "name": "ESNA default EDIT",
        "baseUrl": "https://esna.base.url.edit",
        "authUrl": "https://esna.auth.url.edit",
        "authClientId": "CSC-EDIT",
        "authClientSecret": "ThisIsTheSecret-Edit",
        "authUsername": "csc@projectboston.com.edit",
        "authPassword": "the-password-edit"
      },
      "review" : {
        "reviewer" : [
          "USER_ID1",
          "USER_ID2"
        ],
        "baseUrl": "http://csc.projectboston.dev/review/"
      }
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/esnainstance/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.name" should not contain "ESNA default EDIT"
    And the JSON node "root.baseUrl" should not contain "https://esna.base.url.edit"
    And the JSON node "root.authUrl" should not contain "https://esna.auth.url.edit"
    And the JSON node "root.authClientId" should not contain "CSC-EDIT"
    And the JSON node "root.authClientSecret" should not contain "ThisIsTheSecret-Edit"
    And the JSON node "root.authUsername" should not contain "csc@projectboston.com.edit"
    And the JSON node "root.authPassword" should not contain "the-password-edit"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/allow" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/esnainstance/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.name" should contain "ESNA default EDIT"
    And the JSON node "root.baseUrl" should contain "https://esna.base.url.edit"
    And the JSON node "root.authUrl" should contain "https://esna.auth.url.edit"
    And the JSON node "root.authClientId" should contain "CSC-EDIT"
    And the JSON node "root.authClientSecret" should contain "ThisIsTheSecret-Edit"
    And the JSON node "root.authUsername" should contain "csc@projectboston.com.edit"
    And the JSON node "root.authPassword" should contain "the-password-edit"
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role           |
    | instance_edit  |
    | instance_admin |
    | admin          |

  @instance-delete
  Scenario Outline: Delete a esnainstance which is not used
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[1].id" as placeholder "ID"
    And I send a DELETE request to "/api/esnainstance/ID" with placeholder
    Then the response status code should be <code>
  Examples:
    | role            | code |
    | instance_create | 403  |
    | instance_edit   | 403  |
    | instance_delete | 204  |
    | instance_admin  | 204  |
    | admin           | 204  |

  # DEACTIVATE
  @instance-deactivate @mailer 
  Scenario Outline: Deactivate a esnainstance and deny/allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=instance_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[<key>].id" as placeholder "ID"
    And I send a PUT request to "/api/esnainstance/ID/deactivate" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/esnainstance/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.active" should contain Boolean "true"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/esnainstance/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.active" should contain Boolean "<result>"
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role            | action | result | key |
    | instance_delete | deny   | true   | 3   |
    | instance_admin  | deny   | true   | 3   |
    | admin           | deny   | true   | 3   |
    | instance_delete | allow  | false  | 3   |
    | instance_admin  | allow  | false  | 4   |
    | admin           | allow  | false  | 5   |
