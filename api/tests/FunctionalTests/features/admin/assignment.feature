@assignment
Feature: API Calls for Assignment as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  # READ - LIST
  @assignment-list
  Scenario: Expect a list of assignments
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/assignments?customer=1"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 10 elements
    And the JSON should be valid according to the json schema "tests/json-schema/assignments.json"

  # READ - LIST
  @assignment-list @keep-database
  Scenario: Expect a list of product assignments filtered by product id
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[0].id" as placeholder "PRODUCT_ID"
    When I send a GET request to "/api/assignments?product=PRODUCT_ID" with placeholder
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 1 elements

  # READ - LIST
  @assignment-list @keep-database
  Scenario: Expect a list of product assignments filtered by feature id in additional features
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[6].id" as placeholder "FEATURE_ID"
    When I send a GET request to "/api/assignments?feature=FEATURE_ID" with placeholder
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 10 elements

  # READ - LIST
  @assignment-list @keep-database
  Scenario: Try to get a list of product assignments
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/assignments"
    Then the response status code should be 400

  @assignment-refresh-search-index
  Scenario: Refresh search index
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I am authenticated against esna
    When I send a PUT request to "/api/assignments/ORDER_ID/refresh" with placeholder
    Then the response status code should be 204

  # CREATE
  @assignment-create @keep-database
  Scenario: Create a new assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/customers"
    And I save the value of JSON node "root[0].id" as placeholder "CUSTOMER_ID"
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[0].id" as placeholder "PRODUCT_ID"
    And I send a GET request to "/api/plans"
    And I save the value of JSON node "root[0].id" as placeholder "PLAN_ID"
    And I send a GET request to "/api/esnainstance"
    And I save the value of JSON node "root[0].id" as placeholder "ESNAINSTANCE_ID"
    When I send a POST request to "/api/assignments" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2013-08-24T06:30:08.0Z"
      },
      "plans" : [
        {
          "plan" : {
           "id": "PLAN_ID"
         },
          "duration" : {
            "startDate": "2013-08-24T06:30:08.0Z",
            "endDate": "2013-08-24T06:30:08.0Z"
          }
        }
      ],
      "product" : {
        "id": "PRODUCT_ID"
      },
      "customer" : {
        "id": "CUSTOMER_ID"
      },
      "esnaInstance" : {
        "id": "ESNAINSTANCE_ID"
      },
      "permanentlyDeleteVideosAllowed" : false,
      "name" : "Esna yeah"
    }
    """
    Then the response status code should be 201
    And I send a GET request to "/api/products/PRODUCT_ID" with placeholder
    And the JSON node "root.inUse" should contain Boolean "true"

  # CREATE
  @assignment-create @keep-database
  Scenario: Try to create a new Order
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/assignments" with body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2013-08-24T06:30:08.0Z"
      },
      "product" : {},
      "customer" : {},
      "esnaInstance" : {},
      "name" : ""
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root" should have 10 elements
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].message" should exist

  @assignment-add-feature @keep-database
  Scenario: Try to add a new feature to an existing assignment with custom configurations
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID"
    And I am authenticated against esna
    When I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "customConfigurations" : {
        "wrong.key": "wrong"
      },
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2013-08-24T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    Then the response status code should be 400
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].message" should exist

  @assignment-add-feature
  Scenario: Add a new feature to an existing assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID"
    And the JSON node "root[1].inUse" should contain Boolean "false"
    And I am authenticated against esna
    When I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2013-08-24T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    Then the response status code should be 201
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.additionalFeatures" should have 2 element
    And I send a GET request to "/api/features/FEATURE_ID" with placeholder
    And the JSON node "root.inUse" should contain Boolean "true"

  @assignment-add-feature @keep-database
  Scenario: Add a new feature to an existing assignment with custom configurations
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[2].id" as placeholder "FEATURE_ID"
    And the JSON node "root[2].inUse" should contain Boolean "false"
    And I am authenticated against esna
    When I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "customConfigurations" : {
        "test.test": "test2"
      },
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2013-08-24T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    Then the response status code should be 201
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.additionalFeatures" should have 2 element
    And I send a GET request to "/api/features/FEATURE_ID" with placeholder
    And the JSON node "root.inUse" should contain Boolean "true"

  @assignment-add-feature
  Scenario: Try to add a feature to an existing assignment which is part of the basic product
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "FEATURE_ID"
    When I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2014-08-24T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    Then the response status code should be 400
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].message" should exist

  @assignment-add-feature @keep-database
  Scenario: Try to add a feature to an existing assignment which is already active and is in duration
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID"
    And I am authenticated against esna
    And I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2014-08-24T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    And the response status code should be 201
    And I am authenticated against esna
    When I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-25T06:30:08.0Z",
        "endDate": "2014-08-23T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    Then the response status code should be 400
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].message" should exist

  @assignment-add-feature @keep-database
  Scenario: Add a feature to an existing assignment which is already active and is not in duration
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID"
    And I am authenticated against esna
    And I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2014-08-24T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    And the response status code should be 201
    And I am authenticated against esna
    When I send a POST request to "/api/assignments/ORDER_ID/additional/feature" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2014-08-25T06:30:08.0Z",
        "endDate": "2015-08-23T06:30:08.0Z"
      },
      "feature" : "FEATURE_ID",
      "type": "feature"
    }
    """
    Then the response status code should be 201

  @assignment-deactivate-feature @keep-database
  Scenario: Deactivate a feature
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And I save the value of JSON node "root.additionalFeatures[0].id" as placeholder "FEATURE_ORDER_ADD_ID"
    And I am authenticated against esna
    When I send a DELETE request to "/api/assignments/ORDER_ID/additional/feature/FEATURE_ORDER_ADD_ID" with placeholder
    Then the response status code should be 204
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.additionalFeatures[0].inactive" should contain Boolean "true"
    And the JSON node "root.additionalFeatures[0].inactiveDate" should exist

  @assignment-deactivate-feature @keep-database
  Scenario: Try to deactivate a feature
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a DELETE request to "/api/assignments/ORDER_ID/additional/feature/2"
    Then the response status code should be 404

  @assignment-deactivate @keep-database
  Scenario: Deactivate an assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    When I send a DELETE request to "/api/assignments/ORDER_ID" with placeholder
    Then the response status code should be 204
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.inactive" should contain Boolean "true"
    And the JSON node "root.inactiveDate" should exist

  @assignment-deactivate @keep-database
  Scenario: Try to deactivate an assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a DELETE request to "/api/assignments/4"
    Then the response status code should be 404

  @assignment-add-plan @keep-database
  Scenario: Add a new plan to an existing assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/plans"
    And I save the value of JSON node "root[1].id" as placeholder "PLAN_ID"
    And I am authenticated against esna
    When I send a POST request to "/api/assignments/ORDER_ID/additional/plan" with placeholder and body:
    """
    {
      "duration" : {
        "startDate": "2013-08-24T06:30:08.0Z",
        "endDate": "2014-08-24T06:30:08.0Z"
      },
      "plan" : "PLAN_ID",
      "type": "plan"
    }
    """
    Then the response status code should be 201
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.plans" should have 2 element

  @assignment-deactivate-plan @keep-database
  Scenario: Deactivate a plan
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And I save the value of JSON node "root.plans[0].id" as placeholder "PLAN_ORDER_ADD_ID"
    When I send a DELETE request to "/api/assignments/ORDER_ID/additional/plan/PLAN_ORDER_ADD_ID" with placeholder
    Then the response status code should be 204
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.plans[0].inactive" should contain Boolean "true"
    And the JSON node "root.plans[0].inactiveDate" should exist

  @assignment-renew @keep-database
  Scenario: Renew an assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    When I send a POST request to "/api/assignments/ORDER_ID/renew" with placeholder and body:
    """
    {
      "startDate": "2013-08-24T06:30:08.0Z",
      "endDate": "2013-08-24T06:30:08.0Z"
    }
    """
    Then the response status code should be 201
    And I send a GET request to "/api/assignments/ORDER_ID" with placeholder
    And the JSON node "root.inactive" should contain Boolean "true"
    And the JSON node "root.inactiveDate" should exist
    And I send a GET request to "/api/assignments?customer=1"
    And the JSON node "root" should have 11 elements

  @assignment-playout-script-get @keep-database
  Scenario: Get a playout script for a video manager assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And the "auth" client send a response with:
    """
    {"accessToken":"token"}
    """
    And the "method" client send a response with:
    """
    play-out-script as plain text
    """
    When I send a GET request to "/api/assignments/ORDER_ID/playout-script" with placeholder
    Then the response status code should be 200
    And the JSON node "root.script" should be equal to "play-out-script as plain text"

  @assignment-playout-script-update @keep-database
  Scenario: Update a playout script for a video manager assignment
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/assignments?customer=1"
    And I save the value of JSON node "root[0].id" as placeholder "ORDER_ID"
    And the "auth" client send a response with:
    """
    {"accessToken":"token"}
    """
    And the "method" client send a response with:
    """
    """
    When I send a PUT request to "/api/assignments/ORDER_ID/playout-script" with placeholder and body:
    """
    {"script" : "play-out-script as plain text update"}
    """
    Then print last JSON response
    Then the response status code should be 204
    And the last client request should contain "play-out-script as plain text update" in body

#  @todo fix test
#  @assignment-add-custom-player-skin @assignment-edit
#  Scenario: Add a custom player skin to an existing assignment
#    Given I add "CONTENT_TYPE" header equal to "application/json"
#    And I add "HTTP_ACCEPT" header equal to "application/json"
#    And I set the jwt header
#    And I send a GET request to "/api/assignments?customer=1"
#    And I save the value of JSON node "root[0].id" as placeholder "ASSIGNMENT_ID"
#    And I send a GET request to "/api/player-skins?type=active"
#    And I save the value of JSON node "root[0].id" as placeholder "PLAYER_SKIN_ID"
#    And I am authenticated against esna
#    When I send a POST request to "/api/assignments/ASSIGNMENT_ID/player-skins" with placeholder and body:
#    """
#    {
#        "id": "PLAYER_SKIN_ID"
#    }
#    """
#    And the response status code should be 204
#    And I send a GET request to "/api/assignments/ASSIGNMENT_ID" with placeholder
#    And the JSON node "root.playerSkins" should have 1 elements

