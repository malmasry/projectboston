@feature
Feature: API Calls for Product as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  # READ - LIST
  @feature-list
  Scenario: Expect a list of active features
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    Then the response status code should be 200
    And the JSON should be valid according to the json schema "tests/json-schema/features.json"
    And the JSON node "root" should have 20 elements

  @feature-list @keep-database
  Scenario: Expect a list of all features
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features?type=all"
    Then the response status code should be 200
    And the JSON should be valid according to the json schema "tests/json-schema/features.json"
    And the JSON node "root" should have 30 elements

  # CREATE
  @feature-create @keep-database
  Scenario Outline: Create a new Feature
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/features" with body:
    """
    {
      "name" : {
        "de" : "Erster",
        "en" : "First"
      },
      "configurations": [
        {
          "name": "config_1",
          "value": "config_1_value"
        }
      ],
      "supportedTypes": [1]
    }
    """
    Then the response status code should be <code>
  Examples:
    | role           | code |
    | product_create | 201  |
    | product_admin  | 201  |
    | admin          | 201  |
    | product_edit   | 403  |
    | product_delete | 403  |
    | user           | 403  |

  @feature-create @keep-database
  Scenario: Create a new Feature with custom configurations
    Given I set role "product_create" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/features" with body:
    """
    {
      "name" : {
        "de" : "Erster",
        "en" : "First"
      },
      "configurations": [
        {
          "name": "config_1",
          "value": "config_1_value"
        }
      ],
      "supportedTypes": [1],
      "customConfigurations": [
        {
          "name" : {
            "de" : "CustomConfigTitle",
            "en" : "CustomConfigTitleEN"
          },
          "key" : "custom.config"
        }
      ]
    }
    """
    Then the response status code should be 201

  @feature-create @keep-database
  Scenario:Try to create a new Feature with custom configurations
    Given I set role "product_create" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/features" with body:
    """
    {
      "name" : {
        "de" : "Erster",
        "en" : "First"
      },
      "configurations": [
        {
          "name": "config_1",
          "value": "config_1_value"
        }
      ],
      "supportedTypes": [1],
      "customConfigurations": [
        {
          "key" : "custom.config"
        }
      ]
    }
    """
    Then the response status code should be 400
    And the response should have a violation for "customConfigurations[0].name"

  @feature-create @keep-database
  Scenario: Try to create a new feature with invalid values
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/features" with body:
    """
    {
      "name" : [],
      "supportedTypes":[]
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root" should have 2 elements
    And the response should have a violation for name
    And the response should have a violation for supportedTypes

  @feature-edit @keep-database
  Scenario Outline: Edit an existing feature to add a new translation
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/features/ID" with placeholder and body:
    """
    {
      "id": "ID",
      "name" : {
        "de" : "edit_trans",
        "zz" : "new_trans"
      }
    }
    """
    Then the response status code should be <code>
    And I send a GET request to "/api/features/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.name.zz" should contain "new_trans"
    And the JSON node "root.name.de" should contain "edit_trans"
  Examples:
    | role          | code |
    | product_edit  | 204  |
    | product_admin | 204  |
    | admin         | 204  |

  @feature-edit @keep-database
  Scenario Outline: Try to edit an existing feature to add a new translation
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/features/ID" with placeholder and body:
    """
    {
      "id": "ID",
      "name" : {
        "de" : "edit_trans",
        "zz" : "new_trans"
      }
    }
    """
    Then the response status code should be <code>
  Examples:
    | role           | code |
    | product_create | 403  |
    | product_delete | 403  |
    | user           | 403  |

  @feature-delete @keep-database
  Scenario Outline: Delete a feature which is not used
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "FEATUREID"
    And I send a DELETE request to "/api/features/FEATUREID" with placeholder
    Then the response status code should be <code>
  Examples:
    | role           | code |
    | product_create | 403  |
    | product_edit   | 403  |
    | product_delete | 204  |
    | product_admin  | 204  |
    | admin          | 204  |

  @feature-delete @keep-database
  Scenario Outline: Try to delete a feature which is used
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[11].id" as placeholder "FEATUREID"
    And I send a DELETE request to "/api/features/FEATUREID" with placeholder
    Then the response status code should be 403
  Examples:
    | role           |
    | product_create |
    | product_edit   |
    | product_delete |
    | product_admin  |
    | admin          |

  @feature-deactivate @mailer
  Scenario Outline: Deactivate a feature and deny/allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=product_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[<key>].id" as placeholder "ID"
    And I send a PUT request to "/api/features/ID/deactivate" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/features/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.active" should contain Boolean "true"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/features/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.active" should contain Boolean "<result>"
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role           | action | result | key |
    | product_delete | deny   | true   | 10  |
    | product_admin  | deny   | true   | 10  |
    | admin          | deny   | true   | 10  |
    | product_delete | allow  | false  | 10  |
    | product_admin  | allow  | false  | 11  |
    | admin          | allow  | false  | 12  |

