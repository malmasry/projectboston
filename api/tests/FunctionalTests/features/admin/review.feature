@review
Feature: API Calls for Review as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  # READ - LIST
  @reviewers-list
  Scenario: Get a list of potential reviewers
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/reviewers?action=user_create"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].id" should exist
    And the JSON node "root[0].email" should exist

  # READ - LIST
  @reviewers-list @keep-database
  Scenario: Get a list of potential reviewers with filter
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=user_create"
    And I save the value of JSON node "root[0].id" as placeholder "USER_EXCLUDE"
    When I send a GET request to "/api/reviewers?exclude=USER_EXCLUDE&action=user_create" with placeholder
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 0 elements

  # READ - LIST
  @review-list @keep-database
  Scenario: Get a list of reviews
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/review"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.opener" should have 1 elements
    And the JSON node "root.reviewer" should have 1 elements
    And the JSON node "root.reviewer[0].token" should exist

  # READ
  @review-get @keep-database
  Scenario: Get a detail view of a review
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/review/hashReview1"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.action" should exist
    And the JSON node "root.opener" should exist
    And the JSON node "root.opener.firstName" should exist
    And the JSON node "root.opener.lastName" should exist

  @review-get @keep-database
  Scenario: Try to get a detail view of a review
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/review/hashReview2"
    Then the response status code should be 403