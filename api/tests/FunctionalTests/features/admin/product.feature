@product
Feature: API Calls for Product as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

    # READ - LIST
  @product-list
  Scenario: Expect a list of active products
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 20 elements
    And the JSON node "root[0].name" should exist
    And the JSON node "root[0].name.de" should exist
    And the JSON node "root[0].productType" should exist

  @product-list @keep-database
  Scenario: Expect a list of all products
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products?type=all"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 30 elements
    And the JSON node "root[0].name" should exist
    And the JSON node "root[0].name.de" should exist
    And the JSON node "root[0].productType" should exist

   # CREATE
  @product-create @keep-database
  Scenario Outline: Create a new Product
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "FEATURE_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID2"
    And I send a POST request to "/api/products" with placeholder and body:
    """
    {
      "name" : {
        "de" : "Grundprodukt",
        "en" : "Basic product"
      },
      "features": ["FEATURE_ID1", "FEATURE_ID2"],
      "productType": 1
    }
    """
    Then the response status code should be <code>
  Examples:
    | role           | code |
    | product_create | 201  |
    | product_admin  | 201  |
    | admin          | 201  |
    | product_edit   | 403  |
    | product_delete | 403  |
    | user           | 403  |

  @product-create @keep-database
  Scenario: Try to create a new Product with invalid values
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/products" with body:
    """
    {
      "name" : [],
      "productType":"test"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].message" should exist
    And the JSON node "root[1].property_path" should exist
    And the JSON node "root[1].message" should exist

  @product-create @keep-database
  Scenario: Try to create a new product with invalid features
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/products" with body:
    """
    {
      "name" : {
        "de" : "Grundprodukt",
        "en" : "Basic product"
      },
      "features": ["invalidId", "invalidIdTwo"],
      "productType": 1
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root" should have 2 elements
    And the JSON node "root[0].property_path" should contain "features"
    And the JSON node "root[0].message" should contain "invalidId"
    And the JSON node "root[1].property_path" should contain "features"
    And the JSON node "root[1].message" should contain "invalidIdTwo"

  @product-edit @keep-database
  Scenario Outline: Try to edit an existing product to add a new translation
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/products/ID" with placeholder and body:
    """
    {
      "id" : "ID",
      "name" : {
        "de" : "edit_trans",
        "zz" : "new_trans"
      }
    }
    """
    Then the response status code should be 403
  Examples:
    | role           |
    | product_create |
    | product_delete |
    | user           |

  @product-delete @keep-database
  Scenario Outline: Delete a product which is not used
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[11].id" as placeholder "PRODUCTID"
    And I send a DELETE request to "/api/products/PRODUCTID" with placeholder
    Then the response status code should be <code>
    Examples:
      | role           | code |
      | product_create | 403  |
      | product_edit   | 403  |
      | product_delete | 204  |
      | product_admin  | 204  |
      | admin          | 204  |

  @product-delete @keep-database
  Scenario Outline: Try to delete a product which is used
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[1].id" as placeholder "PRODUCTID"
    And I send a DELETE request to "/api/products/PRODUCTID" with placeholder
    Then the response status code should be 403
    Examples:
      | role           |
      | product_create |
      | product_edit   |
      | product_delete |
      | product_admin  |
      | admin          |

  @product-edit @mailer @keep-database
  Scenario Outline: Edit an existing product to add a new feature and deny the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "FEATURE_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID2"
    And I send a GET request to "/api/reviewers?action=product_edit"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/products/ID" with placeholder and body:
    """
    {
      "product" : {
        "id" : "ID",
        "name" : {
          "de" : "Grundprodukt",
          "en" : "Basic product"
        },
        "features": ["FEATURE_ID1", "FEATURE_ID2"]
      },
      "review" : {
        "reviewer" : [
          "USER_ID1",
          "USER_ID2"
        ],
        "baseUrl": "http://csc.projectboston.dev/review/"
      }
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/products/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.name.de" should not contain "Grundprodukt"
    And the JSON node "root.features" should have 1 elements
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/deny" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/products/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.name.de" should not contain "Grundprodukt"
    And the JSON node "root.features" should have 1 elements
    And the JSON node "root.reviewStatus" should contain "none"
    Examples:
      | role          |
      | product_edit  |
      | product_admin |
      | admin         |

  @product-edit @mailer
  Scenario Outline: Edit an existing product to add a new feature and allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/features"
    And I save the value of JSON node "root[0].id" as placeholder "FEATURE_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "FEATURE_ID2"
    And I send a GET request to "/api/reviewers?action=product_edit"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/products/ID" with placeholder and body:
    """
    {
      "product" : {
        "id" : "ID",
        "name" : {
          "de" : "Grundprodukt",
          "en" : "Basic product"
        },
        "features": ["FEATURE_ID1", "FEATURE_ID2"]
      },
      "review" : {
        "reviewer" : [
          "USER_ID1",
          "USER_ID2"
        ],
        "baseUrl": "http://csc.projectboston.dev/review/"
      }
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/products/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.name.de" should not contain "Grundprodukt"
    And the JSON node "root.features" should have 1 elements
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/allow" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/products/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.name.de" should contain "Grundprodukt"
    And the JSON node "root.name.en" should contain "Basic product"
    And the JSON node "root.features" should have 2 elements
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role          |
    | product_edit  |
    | product_admin |
    | admin         |

  @product-deactivate @mailer
  Scenario Outline: Deactivate a product and deny/allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=product_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[<key>].id" as placeholder "ID"
    And I send a PUT request to "/api/products/ID/deactivate" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/products/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.active" should contain Boolean "true"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/products/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.active" should contain Boolean "<result>"
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role           | action | result | key |
    | product_delete | deny   | true   | 0   |
    | product_admin  | deny   | true   | 0   |
    | admin          | deny   | true   | 0   |
    | product_delete | allow  | false  | 0   |
    | product_admin  | allow  | false  | 1   |
    | admin          | allow  | false  | 2   |