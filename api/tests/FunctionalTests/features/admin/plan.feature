@plan
Feature: API Calls for Plans as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  @plan-list
  Scenario Outline: Expect a list of planss (with roles include read-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/plans"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 10 elements
    And the JSON node "root[0].title" should exist
    And the JSON node "root[9].title" should exist
    Examples:
      | role        |
      | plan_create |
      | plan_read   |
      | plan_edit   |
      | plan_delete |
      | plan_admin  |
      | admin       |

  @plan-list @keep-database
  Scenario: Try to get a list of planss (with roles exclude read-right)
    Given I set role "user" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/plans"
    Then the response status code should be 403

  @plan-create @keep-database
  Scenario: Try to Create a new plans without needed option
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/plans" with body:
    """
    {
      "title" : "Plan-of-admin"
    }
    """
    Then the response status code should be 400
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].message" should exist

  @plan-create @keep-database
  Scenario Outline: Create a new plans with needed and valid option
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/plans" with body:
    """
    {
      "title" : "Plan-of-admin",
      "traffic" : {
        "value" : 2342,
        "unit" : "tb"
      }
    }
    """
    Then the response status code should be 201
  Examples:
    | role        |
    | plan_create |
    | plan_admin  |
    | admin       |

  @plan-create @keep-database
  Scenario: Try to Create a new plans with false unit
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/plans" with body:
    """
    {
      "title" : "Plan-of-admin",
      "traffic" : {
        "value" : 2342,
        "unit" : "gh"
      }
    }
    """
    Then the response status code should be 400
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].property_path" should exist
    And the JSON node "root[0].property_path" should contain "traffic"
    And the JSON node "root[0].message" should exist

  @plan-create @keep-database
  Scenario Outline: Try to create a new plans (with roles exclude create-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/plans" with body:
    """
    {
      "title" : "Plan-of-admin",
      "traffic" : {
        "value" : 2342,
        "unit" : "tb"
      }
    }
    """
    Then the response status code should be 403
  Examples:
    | role        |
    | plan_read   |
    | plan_edit   |
    | plan_delete |
    | user        |

  @plan-edit @keep-database
  Scenario Outline: Try to edit an plan
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/plans"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    When I send a PUT request to "/api/plans/ID" with placeholder and body:
    """
    {
      "id" : "ID",
      "title" : "Plan-of-admin",
      "traffic" : {
        "value" : 2342,
        "unit" : "tb"
      }
    }
    """
    Then the response status code should be 403
  Examples:
    | role        |
    | plan_create |
    | plan_delete |

  @plan-edit @mailer @keep-database
  Scenario Outline: Edit an existing plan and deny the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=plan_edit"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/plans"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    And I send a PUT request to "/api/plans/ID" with placeholder and body:
    """
    {
      "plan" : {
        "id" : "ID",
        "title" : "Plan-of-admin",
        "traffic" : {
          "value" : 2342,
          "unit" : "tb"
        }
      },
      "review" : {
        "reviewer" : [
          "USER_ID1",
          "USER_ID2"
        ],
        "baseUrl": "http://csc.projectboston.dev/review/"
      }
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/plans/ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.title" should not contain "Plan-of-admin"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/deny" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/plans/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.title" should not contain "Plan-of-admin"
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role       |
    | plan_edit  |
    | plan_admin |
    | admin      |

  @plan-edit @mailer
  Scenario Outline: Edit an existing plan and allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=plan_edit"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/plans"
    And I save the value of JSON node "root[0].id" as placeholder "ID"
    When I send a PUT request to "/api/plans/ID" with placeholder and body:
    """
    {
      "plan" : {
        "id" : "ID",
        "title" : "Plan-of-admin",
        "traffic" : {
          "value" : 2342,
          "unit" : "tb"
        }
      },
      "review" : {
        "reviewer" : [
          "USER_ID1",
          "USER_ID2"
        ],
        "baseUrl": "http://csc.projectboston.dev/review/"
      }
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/plans/ID" with placeholder
    And the JSON node "root.title" should not contain "Plan-of-admin"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/allow" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/plans/ID" with placeholder
    And the response status code should be 200
    And the JSON node "root.title" should contain "Plan-of-admin"
    And the JSON node "root.traffic.value" should contain "2342"
    And the JSON node "root.traffic.unit" should contain "tb"
    And the JSON node "root.reviewStatus" should contain "none"
    Examples:
      | role       |
      | plan_edit  |
      | plan_admin |
      | admin      |

  @plan-delete @mailer
  Scenario Outline: Start the delete review for a plans (with roles include delete-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/plans"
    And I save the value of JSON node "root[0].id" as placeholder "PLANID"
    And I send a GET request to "/api/reviewers?action=plan_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a PUT request to "/api/plans/PLANID/review" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/plans/PLANID" with placeholder
    And the response should be in JSON
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/plans/PLANID" with placeholder
    And the response status code should be <code>
  Examples:
    | role        | action | code |
    | plan_delete | allow  | 404  |
    | plan_admin  | allow  | 404  |
    | admin       | allow  | 404  |
    | plan_delete | deny   | 200  |
    | plan_admin  | deny   | 200  |
    | admin       | deny   | 200  |
