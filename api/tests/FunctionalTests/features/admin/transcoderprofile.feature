@transcoderprofile
Feature: API Calls for TranscoderProfiles as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  @transcoderprofile-list
  Scenario: Expect a list of all transcoder profiles
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/transcoderprofiles?type=all"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 30 elements
    And the JSON node "root[0].key" should exist
    And the JSON node "root[0].custom" should exist
    And the JSON node "root[0].active" should exist
    And the JSON node "root[0].inUse" should exist

  @transcoderprofile-list @keep-database
  Scenario: Expect a list of all active transcoder profiles
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/transcoderprofiles?type=active"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 20 elements
    And the JSON node "root[0].key" should exist
    And the JSON node "root[0].custom" should exist

  @transcoderprofile-list @keep-database
  Scenario Outline: Expect a list of transcoder profiles (with roles include read-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/transcoderprofiles?type=all"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 30 elements
    And the JSON node "root[0].key" should exist
    And the JSON node "root[0].custom" should exist
    And the JSON node "root[9].key" should exist
    And the JSON node "root[9].custom" should exist
  Examples:
    | role                     |
    | transcoderprofile_create |
    | transcoderprofile_read   |
    | transcoderprofile_edit   |
    | transcoderprofile_delete |
    | transcoderprofile_admin  |
    | admin                    |

  @transcoderprofile-list @keep-database
  Scenario: Try to get a list of transcoder profiles (with roles exclude read-right)
    Given I set role "user" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/transcoderprofiles"
    Then the response status code should be 400


  @transcoderprofile-delete @keep-database
  Scenario Outline: Delete a transcoder profile (custom)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/transcoderprofiles?type=all"
    And I save the value of JSON node "root[0].key" as placeholder "KEY"
    When I send a DELETE request to "/api/transcoderprofiles/KEY" with placeholder
    Then the response status code should be <code>
  Examples:
    | role                     | code |
    | transcoderprofile_create | 403  |
    | transcoderprofile_edit   | 403  |
    | transcoderprofile_delete | 204  |
    | transcoderprofile_admin  | 204  |
    | admin                    | 204  |

  @transcoderprofile-delete @keep-database
  Scenario Outline: Try to delete a transcoder profile (global)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/transcoderprofiles?type=all"
    And I save the value of JSON node "root[11].key" as placeholder "KEY"
    When I send a DELETE request to "/api/transcoderprofiles/KEY" with placeholder
    Then the response status code should be <code>
  Examples:
    | role                     | code |
    | transcoderprofile_delete | 403  |
    | transcoderprofile_admin  | 403  |
    | admin                    | 403  |

  @transcoderprofile-create @keep-database
  Scenario Outline: Try to create a new transcoder profile
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/transcoderprofiles" with body:
    """
    {
      "custom" : true,
      "keyPrefix": "480p",
      "videoExtension": "mp4",
      "stillExtension": "jpeg",
      "createStill": true,
      "quality" : "480p",
      "video": {
        "codec": {
          "name": "h264",
          "properties": {
            "profile": "main",
            "preset": "fast"
          }
        },
        "resolution": {
          "width": 640,
          "height": 480
        },
        "keyFrame": 50,
        "frameRate": 25,
        "bitRate": 1250,
        "aspectRatio": {
            "width": 4,
            "height": 3
        }
      },
      "audio": {
        "codec": {
          "name": "aac"
        },
        "bitRate": 128,
        "channels": 1,
        "sampleRate": 44100
      }
    }
    """
    Then the response status code should be 403
    Examples:
      | role                     |
      | transcoderprofile_read   |
      | transcoderprofile_edit   |
      | transcoderprofile_delete |
      | user                     |


  @transcoderprofile-create @keep-database
  Scenario: Create a new custom transcoder profile
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/products"
    And I save the value of JSON node "root[0].id" as placeholder "PRODUCT_ID"
    And I send a GET request to "/api/assignments?product=PRODUCT_ID" with placeholder
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].transcoderProfiles" should exist
    And the JSON node "root[0].transcoderProfiles" should have 0 elements
    When I send a POST request to "/api/transcoderprofiles" with body:
    """
    {
      "custom" : true,
      "keyPrefix": "480p",
      "videoExtension": "mp4",
      "stillExtension": "jpeg",
      "createStill": true,
      "quality" : "480p",
      "video": {
        "codec": {
          "name": "h264",
          "properties": {
            "profile": "main",
            "preset": "fast"
          }
        },
        "resolution": {
          "width": 640,
          "height": 480
        },
        "keyFrame": 50,
        "frameRate": 25,
        "bitRate": 1250,
        "aspectRatio": {
            "width": 4,
            "height": 3
        }
      },
      "audio": {
        "codec": {
          "name": "aac"
        },
        "channels": 2,
        "bitRate": 128,
        "sampleRate": 44100
      }
    }
    """
    Then the response status code should be 201
    And I send a GET request to "/api/assignments?product=PRODUCT_ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 1 elements
    And the JSON node "root[0].transcoderProfiles" should exist
    And the JSON node "root[0].transcoderProfiles" should have 0 elements

  @transcoderprofile-create
  Scenario Outline: Create a new transcoder profile (with roles include create-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/transcoderprofiles" with body:
    """
    {
      "custom" : true,
      "keyPrefix": "480p",
      "videoExtension": "mp4",
      "stillExtension": "jpeg",
      "createStill": true,
      "quality" : "480p",
      "video": {
        "codec": {
          "name": "h264",
          "properties": {
            "profile": "main",
            "preset": "fast"
          }
        },
        "resolution": {
          "width": 640,
          "height": 480
        },
        "keyFrame": 50,
        "frameRate": 25,
        "bitRate": 1250,
        "aspectRatio": {
            "width": 4,
            "height": 3
        }
      },
      "audio": {
        "codec": {
          "name": "aac"
        },
        "channels": 1,
        "bitRate": 128,
        "sampleRate": 44100
      }
    }
    """
    Then the response status code should be 201
    Examples:
      | role                     |
      | transcoderprofile_create |
      | transcoderprofile_admin  |
      | admin                    |

  @transcoderprofile-deactivate @mailer
  Scenario Outline: Deactivate a transcoder profile and deny/allow the action
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=transcoderprofile_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a GET request to "/api/transcoderprofiles?type=all"
    And I save the value of JSON node "root[20].key" as placeholder "KEY"
    When I send a PUT request to "/api/transcoderprofiles/KEY/deactivate" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/transcoderprofiles/KEY" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.active" should contain Boolean "true"
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/transcoderprofiles/KEY" with placeholder
    And the response status code should be 200
    And the JSON node "root.active" should contain Boolean "<result>"
    And the JSON node "root.reviewStatus" should contain "none"
  Examples:
    | role                     | action | result |
    | transcoderprofile_delete | deny   | true   |
    | transcoderprofile_admin  | deny   | true   |
    | admin                    | deny   | true   |
    | transcoderprofile_delete | allow  | false  |
    | transcoderprofile_admin  | allow  | false  |
    | admin                    | allow  | false  |



  # @todo reactive when fake responses for OAuth and Esna are available
#  @transcoderprofile-create
#  Scenario: Create a new global transcoder profile
#    Given I add "CONTENT_TYPE" header equal to "application/json"
#    And I add "HTTP_ACCEPT" header equal to "application/json"
#    And I set the jwt header
#    And I send a GET request to "/api/products"
#    And I save the value of JSON node "root[0].id" as placeholder "PRODUCT_ID"
#    And I send a GET request to "/api/assignments?product=PRODUCT_ID" with placeholder
#    And the JSON node "root" should have 1 elements
#    And the JSON node "root[0].transcoderProfiles" should exist
#    And the JSON node "root[0].transcoderProfiles" should have 0 elements
#    When I send a POST request to "/api/transcoderprofiles" with body:
#    """
#    {
#      "custom" : false,
#      "keyPrefix": "480p-global",
#      "videoExtension": "mp4",
#      "stillExtension": "jpeg",
#      "createStill": true,
#      "quality" : "480p",
#      "video": {
#        "codec": {
#          "name": "h264",
#          "properties": {
#            "profile": "main",
#            "preset": "fast"
#          }
#        },
#        "resolution": {
#          "width": 640,
#          "height": 480
#        },
#        "keyFrame": 50,
#        "frameRate": 25,
#        "bitRate": 1250,
#        "aspectRatio": {
#            "width": 4,
#            "height": 3
#        }
#      },
#      "audio": {
#        "codec": {
#          "name": "aac"
#        },
#        "channels" : 2,
#        "bitRate": 128,
#        "sampleRate": 44100
#      }
#    }
#    """
#    Then the response status code should be 201
#    And I send a GET request to "/api/assignments?product=PRODUCT_ID" with placeholder
#    And the response status code should be 200
#    And the response should be in JSON
#    And the JSON node "root" should have 1 elements
#    And the JSON node "root[0].transcoderProfiles" should exist
#    And the JSON node "root[0].transcoderProfiles" should have 1 elements

  # @todo reactive when fake responses for OAuth and Esna are available
#  @transcoderprofile-edit
#  Scenario Outline: Edit an existing transcoder profile to become custom (with roles include edit-right)
#    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
#    And I add "CONTENT_TYPE" header equal to "application/json"
#    And I add "HTTP_ACCEPT" header equal to "application/json"
#    And I set the jwt header
#    And I send a GET request to "/api/transcoderprofiles?type=all"
#    And I save the value of JSON node "root[0].key" as placeholder "KEY"
#    When I send a PUT request to "/api/transcoderprofiles/KEY" with placeholder and body:
#    """
#    {
#      "key": "KEY",
#      "custom" : true,
#      "videoExtension": "mp4",
#      "stillExtension": "jpeg",
#      "createStill": true,
#      "quality": "480p",
#      "video": {
#        "codec": {
#          "name": "h264",
#          "properties": {
#            "profile": "main",
#            "preset": "fast"
#          }
#        },
#        "resolution": {
#          "width": 640,
#          "height": 480
#        },
#        "keyFrame": 50,
#        "frameRate": 25,
#        "bitRate": 1250,
#        "aspectRatio": {
#            "width": 4,
#            "height": 3
#        }
#      },
#      "audio": {
#        "codec": {
#          "name": "aac"
#        },
#        "channels" : 2,
#        "bitRate": 128,
#        "sampleRate": 44100
#      }
#    }
#    """
#    Then the response status code should be 204
#    And I send a GET request to "/api/transcoderprofiles/KEY" with placeholder
#    And the response status code should be 200
#    And the response should be in JSON
#    And the JSON node "root.custom" should contain Boolean "true"
#  Examples:
#    | role                    |
#    | transcoderprofile_edit  |
#    | transcoderprofile_admin |
#    | admin                   |

  # @todo reactive when fake responses for OAuth and Esna are available
#  @transcoderprofile-edit
#  Scenario Outline: Try to edit an existing transcoder profile to become custom (with roles exclude edit-right)
#    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
#    And I add "CONTENT_TYPE" header equal to "application/json"
#    And I add "HTTP_ACCEPT" header equal to "application/json"
#    And I set the jwt header
#    And I send a GET request to "/api/transcoderprofiles?type=all"
#    And I save the value of JSON node "root[0].key" as placeholder "KEY"
#    When I send a PUT request to "/api/transcoderprofiles/KEY" with placeholder and body:
#    """
#    {
#      "key": "KEY",
#      "custom" : true,
#      "videoExtension": "mp4",
#      "stillExtension": "jpeg",
#      "createStill": true,
#      "quality": "480p",
#      "video": {
#        "codec": {
#          "name": "h264",
#          "properties": {
#            "profile": "main",
#            "preset": "fast"
#          }
#        },
#        "resolution": {
#          "width": 640,
#          "height": 480
#        },
#        "keyFrame": 50,
#        "frameRate": 25,
#        "bitRate": 1250,
#        "aspectRatio": {
#            "width": 4,
#            "height": 3
#        }
#      },
#      "audio": {
#        "codec": {
#          "name": "aac"
#        },
#        "channels" : 2,
#        "bitRate": 128,
#        "sampleRate": 44100
#      }
#    }
#    """
#    Then the response status code should be 403
#  Examples:
#    | role                     |
#    | transcoderprofile_create |
#    | transcoderprofile_read   |
#    | transcoderprofile_delete |

  # @todo reactive when fake responses for OAuth and Esna are available
#  @transcoderprofile-edit
#  Scenario: Edit an existing transcoder profile to become custom
#    Given I add "CONTENT_TYPE" header equal to "application/json"
#    And I add "HTTP_ACCEPT" header equal to "application/json"
#    And I set the jwt header
#    And I send a GET request to "/api/transcoderprofiles?type=all"
#    And I save the value of JSON node "root[0].key" as placeholder "KEY"
#    When I send a PUT request to "/api/transcoderprofiles/KEY" with placeholder and body:
#    """
#    {
#      "key": "KEY",
#      "custom" : true,
#      "videoExtension": "mp4",
#      "stillExtension": "jpeg",
#      "createStill": true,
#      "quality": "480p",
#      "video": {
#        "codec": {
#          "name": "h264",
#          "properties": {
#            "profile": "main",
#            "preset": "fast"
#          }
#        },
#        "resolution": {
#          "width": 640,
#          "height": 480
#        },
#        "keyFrame": 50,
#        "frameRate": 25,
#        "bitRate": 1250,
#        "aspectRatio": {
#            "width": 4,
#            "height": 3
#        }
#      },
#      "audio": {
#        "codec": {
#          "name": "aac"
#        },
#        "channels" : 2,
#        "bitRate": 128,
#        "sampleRate": 44100
#      }
#    }
#    """
#    Then the response status code should be 204
#    And I send a GET request to "/api/transcoderprofiles/KEY" with placeholder
#    And the response status code should be 200
#    And the response should be in JSON
#    And the JSON node "root.custom" should contain Boolean "true"

  # @todo reactive when fake responses for OAuth and Esna are available
#  @transcoderprofile-edit
#  Scenario: Edit an existing transcoder profile to become default
#    Given I add "CONTENT_TYPE" header equal to "application/json"
#    And I add "HTTP_ACCEPT" header equal to "application/json"
#    And I set the jwt header
#    And I send a GET request to "/api/transcoderprofiles?type=all"
#    And I save the value of JSON node "root[0].key" as placeholder "KEY"
#    When I send a PUT request to "/api/transcoderprofiles/KEY" with placeholder and body:
#    """
#    {
#      "key": "KEY",
#      "custom" : false,
#      "videoExtension": "mp4",
#      "stillExtension": "jpeg",
#      "createStill": true,
#      "quality": "480p",
#      "video": {
#        "codec": {
#          "name": "h264",
#          "properties": {
#            "profile": "main",
#            "preset": "fast"
#          }
#        },
#        "resolution": {
#          "width": 640,
#          "height": 480
#        },
#        "keyFrame": 50,
#        "frameRate": 25,
#        "bitRate": 1250,
#        "aspectRatio": {
#            "width": 4,
#            "height": 3
#        }
#      },
#      "audio": {
#        "codec": {
#          "name": "aac"
#        },
#        "channels" : 2,
#        "bitRate": 128,
#        "sampleRate": 44100
#      }
#    }
#    """
#    Then the response status code should be 204
#    And I send a GET request to "/api/transcoderprofiles/KEY" with placeholder
#    And the response status code should be 200
#    And the response should be in JSON
#    And the JSON node "root.custom" should contain Boolean "false"
