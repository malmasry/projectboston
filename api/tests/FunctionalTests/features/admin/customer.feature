@customer
Feature: API Calls for Customer as admin

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  # READ
  @customer-get
  Scenario: Get a detail view of a customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/customers/2"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "type" should exist
    And the JSON node "address.street" should exist

  # READ - LIST
  @customer-list @keep-database
  Scenario: Expect a list of customers
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/customers"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 13 elements
    And the JSON node "root[0].name" should exist
    And the JSON node "root[0].type" should exist
    And the JSON node "root[1].name" should exist
    And the JSON node "root[1].type" should exist
    And the JSON node "root[2].name" should exist
    And the JSON node "root[2].type" should exist
    And the JSON node "root[12].name" should exist
    And the JSON node "root[12].type" should exist

  @customer-list @keep-database
  Scenario: Expect a list of customers with filter
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/customers?parent=2"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 6 elements
    And the JSON node "root[0].name" should exist
    And the JSON node "root[0].type" should exist
    And the JSON node "root[1].name" should exist
    And the JSON node "root[1].type" should exist
    And the JSON node "root[2].name" should exist
    And the JSON node "root[2].type" should exist
    And the JSON node "root[5].name" should exist
    And the JSON node "root[5].type" should exist

  # UPDATE
  @customer-edit @keep-database
  Scenario: Edit an existing Customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a PUT request to "/api/customers/1" with body:
    """
    {
      "id" : "1",
      "name" : "Unternehmensname",
      "street" : "Straße",
      "zip": "10245",
      "city": "Berlin",
      "country": "DE",
      "phone": "+49 96 86 5965",
      "fax": "+49 96 86 5965",
      "type":"partner",
      "contact" : {
        "title":"Dr.",
        "salutation":"Herr",
        "firstName": "Nameaa",
        "lastName":"Name",
        "email": "test@projectboston.com",
        "phone": "+49 96 86 5965",
        "fax": "+49 96 86 5965"
      }
    }
    """
    Then the response status code should be 204

  @customer-delete @mailer
  Scenario Outline: Start the delete review for a customer (with roles include delete-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=customer_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a PUT request to "/api/customers/2/review" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And the response should be in JSON
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/customers/2"
    And the response should be in JSON
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/customers/2"
    And the response status code should be <code>
  Examples:
    | role            | action | code |
    | customer_delete | allow  | 404  |
    | customer_admin  | allow  | 404  |
    | admin           | allow  | 404  |
    | customer_delete | deny   | 200  |
    | customer_admin  | deny   | 200  |
    | admin           | deny   | 200  |

  @customer-delete @keep-database
  Scenario: Try to start delete review for customer (myself)
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/reviewers?action=customer_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    When I send a PUT request to "/api/customers/1/review" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 403

  # CREATE
  @customer-create @keep-database
  Scenario: Create a new minimal customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/customers" with body:
    """
    {
      "name" : "Unternehmensname",
      "type":"partner",
      "parent":{
        "id": "1"
      }
    }
    """
    Then the response status code should be 201

  @customer-create @keep-database
  Scenario: Create a new Customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/customers" with body:
    """
    {
      "name" : "FakeName",
      "address": {
        "street" : "Straße",
        "zip": "10256",
        "city": "Berlin",
        "country": "DE"
      },
      "phone": "+49 96 86 5965",
      "fax": "+49 96 86 5965",
      "type":"partner",
      "parent":{
        "id": "1"
      },
      "contact" : {
        "title":"Dr.",
        "salutation":"Herr",
        "firstName": "Test",
        "lastName":"Name",
        "email": "test@projectboston.com",
        "phone": "+49 96 86 5965",
        "fax": "+49 96 86 5965"
      }
    }
    """
    Then the response status code should be 201

  @customer-create @keep-database
  Scenario: Try to create a new customer with invalid values
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/customers" with body:
    """
    {
      "name" : "",
      "address": {
        "street" : "",
        "zip": "",
        "city": "",
        "country": "Deutschland"
      },
        "phone": "024156488",
        "fax": "0354649974",
      "type":"dummy",
      "contact" : {
        "title":"",
        "salutation":"",
        "firstName": "",
        "lastName":"",
        "email": "testprojectboston.com",
        "phone": "024156488",
        "fax": "0354649974"
      },
      "parent":{
        "id": "1"
      }
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root" should have 8 elements
    And the JSON node "root[0].property_path" should contain "name"
    And the JSON node "root[1].property_path" should contain "type"
    And the JSON node "root[2].property_path" should contain "contact.email"
    And the JSON node "root[3].property_path" should contain "contact.phone"
    And the JSON node "root[4].property_path" should contain "contact.fax"
    And the JSON node "root[5].property_path" should contain "address.country"
    And the JSON node "root[6].property_path" should contain "phone"
    And the JSON node "root[7].property_path" should contain "fax"
