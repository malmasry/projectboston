@log
Feature: API Calls for Log

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"


  Scenario: Expect a list of log entries for the customer of the user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/logs"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON should be valid according to the json schema "tests/json-schema/logs.json"


