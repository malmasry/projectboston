@common
Feature: Api Calls for common

  Scenario: Expect a list of countries
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a GET request to "/api/common/countries"

    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root[2].name" should exist
    And the JSON node "root[2].name" should contain "Albania"

  @keep-database
  Scenario: Expect a list of localized countries
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a GET request to "/api/common/countries?locale=de"

    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root[3].name" should exist
    And the JSON node "root[3].name" should contain "Albanien"

  @keep-database
  Scenario: Expect a list of locales
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a GET request to "/api/common/locales"

    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root[7].name" should exist
    And the JSON node "root[7].name" should contain "Albanian"

  @keep-database
  Scenario: Expect a list of localized locales
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a GET request to "/api/common/locales?locale=de"

    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root[5].name" should exist
    And the JSON node "root[5].name" should contain "Albanisch"

