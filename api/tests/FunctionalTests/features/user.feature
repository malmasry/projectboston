@user
Feature: API Calls for User

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  @user-create @mailer
  Scenario: Create a new users
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/users" with body:
    """
    {
        "email": "test@projectboston.com",
        "firstName": "Vorname",
        "lastName": "Nachname",
        "locale": "de",
        "customer" : {
          "id" :2
        },
        "baseUrl" : "https://uri.local"
    }
    """
    Then the response status code should be 201
    Then I should get an email on "test@projectboston.com" with:
    """
    https://uri.local
    """

  @user-create @keep-database
  Scenario: Try to create a new users
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a POST request to "/api/users" with body:
    """
    {
        "email": "testprojectboston.com",
        "firstName": "",
        "lastName": "",
        "locale": "",
        "customer" : {
          "id": "2"
        },
        "baseUrl" : "ftp://test.de"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root[0].property_path" should contain "firstName"
    And the JSON node "root[1].property_path" should contain "lastName"
    And the JSON node "root[2].property_path" should contain "email"
    And the JSON node "root[3].property_path" should contain "locale"
    And the JSON node "root[4].property_path" should contain "baseUrl"

  @user-create @keep-database
  Scenario: Try to create a second user with same email addy
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/users" with body:
    """
    {
        "email": "test2@projectboston.com",
        "firstName": "Vorname",
        "lastName": "Nachname",
        "locale": "de",
        "customer" : {
          "id" :2
        },
        "baseUrl" : "https://uri.local"
    }
    """
    And the response status code should be 201
    When I send a POST request to "/api/users" with body:
    """
    {
        "email": "test2@projectboston.com",
        "firstName": "Vorname",
        "lastName": "Nachname",
        "locale": "de",
        "customer" : {
          "id" :2
        },
        "baseUrl" : "https://uri.local"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root[0].property_path" should contain "email"

  @user-list @keep-database
  Scenario: Expect a list of users
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/users"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root[0].id" should exist
    And the JSON node "root[0].firstName" should exist
    And the JSON node "root[0].lastName" should exist
    And the JSON node "root[0].email" should exist

  @user-list @keep-database
  Scenario: Expect a list of users with filter
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/users?customer=2"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root[0].id" should exist
    And the JSON node "root[0].firstName" should exist
    And the JSON node "root[0].lastName" should exist
    And the JSON node "root[0].email" should exist

  @user-delete @keep-database
  Scenario: Try to start delete review for a non existing user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a PUT request to "/api/users/343434/review"
    Then the response status code should be 404

  @user-delete @keep-database
  Scenario: Try to start delete review with user without right
    Given I log in as "user_without_admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And I save the value of JSON node "root[2].id" as placeholder "USER_DELETE_ID"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a PUT request to "/api/users/USER_DELETE_ID/review" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 403

  @user-role-get @keep-database
  Scenario: Get the role structure for frontend editing
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/security/roles"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.user" should have 5 elements
    And the JSON node "root.customer" should have 4 elements
    And the JSON node "root.admin" should have 1 elements

  @user-role-edit @keep-database
  Scenario: Edit the roles of a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[2].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/USER_ID/roles" with placeholder and body:
    """
    {
      "id" : "USER_ID",
      "roles" : [
        "user_edit",
        "customer_create"
      ],
      "firstName" : "dummy"
    }
    """
    Then the response status code should be 204
    And I send a GET request to "/api/users/USER_ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.roles" should have 2 elements
    And the JSON node "root.firstName" should not contain "dummy"
    And the JSON node "root.roles[0]" should contain "user_edit"
    And the JSON node "root.roles[1]" should contain "customer_create"

  @user-role-edit @keep-database
  Scenario: Try to edit the roles of a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[2].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/USER_ID/roles" with placeholder and body:
    """
    {
      "id" : "USER_ID",
      "roles" : [
        "user_edit",
        "customer_create",
        "test"
      ]
    }
    """
    Then the response status code should be 400
    And the JSON node "root[0].message" should contain "test"

  @user-role-edit @keep-database
  Scenario: Try to edit the roles of a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[2].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/343434/roles" with placeholder and body:
    """
    {
      "id" : "USER_ID",
      "roles" : [
        "user_edit",
        "customer_create",
        "test"
      ]
    }
    """
    Then the response status code should be 404

  @user-lock @keep-database
  Scenario: Lock a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/USER_ID/lock" with placeholder
    Then the response status code should be 204
    And I send a GET request to "/api/users/USER_ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.locked" should contain Boolean "true"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I add "HTTP_AUTHORIZATION" header equal to " "
    And I send a POST request to "/api/security/login" with body:
    """
    {
        "email": "Consumer@csc.projectboston.dev",
        "password": "test"
    }
    """
    And the response status code should be 401
    And the header "projectboston-reason" should be equal to "locked"

  @user-lock @keep-database
  Scenario: Try to lock the current user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/USER_ID/lock" with placeholder
    Then the response status code should be 403
  @user-lock @keep-database
  Scenario: Try to unlock the current user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/USER_ID/unlock" with placeholder
    Then the response status code should be 403

  @user-edit @keep-database
  Scenario: Edit the basic data of a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[2].id" as placeholder "USER_ID"
    When I send a PUT request to "/api/users/USER_ID" with placeholder and body:
    """
    {
        "id": "USER_ID",
        "firstName": "Vorname",
        "lastName": "Nachname",
        "locale": "de"
    }
    """
    Then the response status code should be 204
    And I send a GET request to "/api/users/USER_ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.firstName" should contain "Vorname"
    And the JSON node "root.lastName" should contain "Nachname"
    And the JSON node "root.locale" should contain "de"

  @user-delete @mailer
  Scenario Outline: Start the delete review for a customer (with roles include delete-right)
    Given I set role "<role>" for "user_without_admin@csc.projectboston.dev" with "test" as "Admin@csc.projectboston.dev" with "test"
    And I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And I save the value of JSON node "root[2].id" as placeholder "USER_DELETE_ID"
    And I send a GET request to "/api/reviewers?action=user_delete"
    And I save the value of JSON node "root[0].id" as placeholder "USER_ID1"
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID2"
    And I send a PUT request to "/api/users/USER_DELETE_ID/review" with placeholder and body:
    """
    {
      "reviewer" : [
        "USER_ID1",
        "USER_ID2"
      ],
      "baseUrl": "http://csc.projectboston.dev/review/"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get an email on "Admin@csc.projectboston.dev" with:
    """
    http://csc.projectboston.dev/review/
    """
    And I should get the "REVIEW_TOKEN" from an email on "Admin@csc.projectboston.dev" with base url "http://csc.projectboston.dev/review/"
    And I send a GET request to "/api/users/USER_DELETE_ID" with placeholder
    And the response should be in JSON
    And the JSON node "root.reviewStatus" should contain "under_review"
    And I log in as "Admin@csc.projectboston.dev" with "test"
    And I set the jwt header
    And I send a PUT request to "/api/review/REVIEW_TOKEN/<action>" with placeholder
    And the response status code should be 204
    And I send a GET request to "/api/users/USER_DELETE_ID" with placeholder
    And the response status code should be <code>
  Examples:
    | role        | action | code |
    | user_delete | allow  | 404  |
    | user_admin  | allow  | 404  |
    | admin       | allow  | 404  |
    | user_delete | deny   | 200  |
    | user_admin  | deny   | 200  |
    | admin       | deny   | 200  |

  @user-lock @mailer @keep-database
  Scenario: Unlock a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And the response status code should be 200
    And the response should be in JSON
    And I save the value of JSON node "root[1].id" as placeholder "USER_ID"
    And I save the value of JSON node "root[1].email" as placeholder "USER_EMAIL"
    When I send a PUT request to "/api/users/USER_ID/unlock" with placeholder
    Then the response status code should be 204
    And I should get an email on "USER_EMAIL" with:
    """
    UNLOCKED
    """
    And I send a GET request to "/api/users/USER_ID" with placeholder
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.locked" should contain Boolean "false"

