@profile
Feature: API Calls for Profile

  Background:
    Given I log in as "Admin@csc.projectboston.dev" with "test"

  @profile-set-password
  Scenario: Try to set a new password with invalid current password
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a PUT request to "/api/profile/password" with body:
    """
    {
        "plainPassword": "test2",
        "repeatedPlainPassword": "test2",
        "current": "tes65544t"
    }
    """
    Then the response status code should be 400

  @profile-edit-data @keep-database
  Scenario: Edit the basic data of a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/users"
    And I find Id for "Admin@csc.projectboston.dev"
    When I send a PUT request to "/api/profile/data" with placeholder and body:
    """
    {
        "firstName": "Vor name",
        "lastName": "name",
        "locale": "de",
        "id": "LOGGEDIN_USER_ID"
    }
    """
    Then the response status code should be 204
    And I send a GET request to "/api/profile"
    And the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.firstName" should contain "Vorname"

  @profile-get @keep-database
  Scenario: Get complete data of current user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/profile"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root.reviewCount" should exist

  @profile-set-password @keep-database
  Scenario: Set a new password
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a PUT request to "/api/profile/password" with body:
    """
    {
        "plainPassword": "test2",
        "repeatedPlainPassword": "test2",
        "current": "test"
    }
    """
    Then the response status code should be 204
    And I send a POST request to "/api/security/login" with body:
    """
    {
        "email": "admin@csc.projectboston.dev",
        "password": "test2"
    }
    """
    And the response status code should be 200
