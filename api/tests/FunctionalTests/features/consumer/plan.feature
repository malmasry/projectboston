@plan
Feature: API Calls for Plans as consumer

  Background:
    Given I log in as "Consumer@csc.projectboston.dev" with "test"

  @plan-list
  Scenario: Try to expect a list of plans (as ConsumerCustomer)
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/plans"
    Then the response status code should be 403

  @plan-create @keep-database
  Scenario: Try to create a new plan
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/plans" with body:
    """
    {
      "title" : "Plan-of-admin",
      "traffic" : {
        "value" : 2342,
        "unit" : "tb"
      }
    }
    """
    Then the response status code should be 403

