@transcoderprofile
Feature: API Calls for TranscoderProfiles as consumer

  Background:
    Given I log in as "Consumer@csc.projectboston.dev" with "test"

  @transcoderprofile-list
  Scenario: Try to expect a list of transcoder profiles (as ConsumerCustomer)
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a GET request to "/api/transcoderprofiles?type=all"
    Then the response status code should be 403

  @transcoderprofile-create @keep-database
  Scenario: Try to create a new transcoder profile
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/transcoderprofiles" with body:
    """
    {
      "custom" : true,
      "keyPrefix": "480p",
      "videoExtension": "mp4",
      "stillExtension": "jpeg",
      "createStill": true,
      "quality" : "480p",
      "video": {
        "codec": {
          "name": "h264",
          "properties": {
            "profile": "main",
            "preset": "fast"
          }
        },
        "resolution": {
          "width": 640,
          "height": 480
        },
        "keyFrame": 50,
        "frameRate": 25,
        "bitRate": 1250,
        "aspectRatio": {
            "width": 4,
            "height": 3
        }
      },
      "audio": {
        "codec": {
          "name": "aac"
        },
        "channels": 2,
        "bitRate": 128,
        "sampleRate": 44100
      }
    }
    """
    Then the response status code should be 403
