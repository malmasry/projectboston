@customer
Feature: API Calls for Customer as consumer

  Background:
    Given I log in as "Consumer@csc.projectboston.dev" with "test"

  # CREATE
  @customer-create
  Scenario: Try to create a new Customer with valid data
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a POST request to "/api/customers" with body:
    """
    {
      "name" : "SomFakeUser",
      "street" : "Straße",
      "zip": "10552",
      "city": "Berlin",
      "country": "DE",
      "phone": "+49 96 86 5965",
      "fax": "+49 96 86 5965",
      "type":"partner",
      "parent":{
        "id": "1"
      },
      "contact" : {
        "title":"Dr.",
        "salutation":"Herr",
        "firstName": "Test",
        "lastName":"Name",
        "email": "test@projectboston.com",
        "phone": "+49 96 86 5965",
        "fax": "+49 96 86 5965"
      }
    }
    """
    Then the response status code should be 403

  # READ
  @customer-get @keep-database
  Scenario: Get a detail view of a customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/customers/3"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "type" should exist
    And the JSON node "address.street" should exist

  @customer-get @keep-database
  Scenario: Try to get a detail view of a customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/customers/1"
    Then the response status code should be 403

  # READ - LIST
  @customer-list @keep-database
  Scenario: Expect a list of customers without filter
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/customers"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 1 elements

  @customer-list @keep-database
  Scenario: Expect a list of customers with valid filter
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/customers?parent=3"
    Then the response status code should be 200
    And the response should be in JSON
    And the JSON node "root" should have 0 elements

  @customer-list @keep-database
  Scenario: Try to get a list of customers with invalid filter
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    When I send a GET request to "/api/customers?parent=1"
    Then the response status code should be 403

  # UPDATE
  @customer-edit @keep-database
  Scenario: Try to edit an existing Customer
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I set the jwt header
    And I send a PUT request to "/api/customers/1" with body:
    """
    {
     "id" : "1",
      "name" : "Unternehmensname",
      "street" : "Straße",
      "zip": "10245",
      "city": "Berlin",
      "country": "DE",
      "phone": "+41 562 44 668 18 00",
      "fax": "+41 562 668 18 00",
      "type":"partner",
      "contact" : {
        "title":"Dr.",
        "salutation":"Herr",
        "firstName": "Test",
        "lastName":"Name",
        "email": "test@projectboston.com",
        "phone": "+49 96 86 5965",
        "fax": "+49 96 86 5965"
      }
    }
    """
    Then the response status code should be 403