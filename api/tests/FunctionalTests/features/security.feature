@security
Feature: Api Calls for security

  @security-login
  Scenario: login in a user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I add "HTTP_AUTHORIZATION" header equal to " "
    When I send a POST request to "/api/security/login" with body:
    """
    {
        "email": "test@csc.projectboston.dev",
        "password": "test"
    }
    """
    Then the response status code should be 200

  @security-login @keep-database
  Scenario: login without values
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a POST request to "/api/security/login" with body:
    """
    {
        "email": "",
        "password": ""
    }
    """
    Then the response status code should be 401

  @security-login @keep-database
  Scenario: login with none existent user
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a POST request to "/api/security/login" with body:
    """
    {
        "email": "falseEmail",
        "password": "test"
    }
    """
    Then the response status code should be 401

  @security-login @keep-database
  Scenario: login with false password
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a POST request to "/api/security/login" with body:
    """
    {
        "email": "test@csc.projectboston.dev",
        "password": "test2"
    }
    """
    Then the response status code should be 401

  @security-reset-password @keep-database
  Scenario: Set a new password for an given hash
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a POST request to "/api/security/reset-password/hashCustom1" with body:
    """
    {
        "plainPassword": "test33",
        "repeatedPlainPassword": "test33"
    }
    """
    Then the response status code should be 204

  @security-reset-password
  Scenario: Set a new password for an given hash with invalid password
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a POST request to "/api/security/reset-password/hashCustom1" with body:
    """
    {
        "plainPassword": "test",
        "repeatedPlainPassword": "t2est"
    }
    """
    Then the response status code should be 400
    And the response should be in JSON
    And the JSON node "root[0].message" should contain "test"

  @security-reset-password
  Scenario: Set a new password for an given hash with invalid password and try again
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    And I send a POST request to "/api/security/reset-password/hashCustom1" with body:
    """
    {
        "plainPassword": "test",
        "repeatedPlainPassword": "t2est"
    }
    """
    And the response status code should be 400
    When I send a POST request to "/api/security/reset-password/hashCustom1" with body:
    """
    {
        "plainPassword": "test",
        "repeatedPlainPassword": "t2est"
    }
    """
    Then print last JSON response
    Then the response status code should be 404

  @mailer @security-request-password @keep-database
  Scenario: Request a new password for a valid email address
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a PUT request to "/api/security/request-password/test%40csc.projectboston.dev" with body:
    """
    {
        "baseUrl" : "ftp://test.de"
    }
    """
    Then the response status code should be 204
    And I should get an email on "test@csc.projectboston.dev" with:
    """
    ftp://test.de
    """

  @security-request-password @keep-database
  Scenario: Request a new password for a valid nonexisting email address
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a PUT request to "/api/security/request-password/notexist%40csc.projectboston.dev"
    Then the response status code should be 204

  @security-request-password @keep-database
  Scenario: Request a new password for a invalid email address
    Given I add "CONTENT_TYPE" header equal to "application/json"
    And I add "HTTP_ACCEPT" header equal to "application/json"
    When I send a PUT request to "/api/security/request-password/invalidemail"
    Then the response status code should be 204
