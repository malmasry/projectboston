<?php

$set = new h4cc\AliceFixturesBundle\Fixtures\FixtureSet(array(
    'locale' => 'de_DE',
    'seed' => 42,
    'do_drop' => true,
    'do_persist' => true,
));

$set->addFile(__DIR__.'/Security.yml', 'yaml');
$set->addFile(__DIR__.'/Customer.yml', 'yaml');
$set->addFile(__DIR__.'/PlayerSkin.yml', 'yaml');
$set->addFile(__DIR__.'/DefaultTranscoderProfile.yml', 'yaml');
$set->addFile(__DIR__.'/EsnaDevSystem.yml', 'yaml');

return $set;
