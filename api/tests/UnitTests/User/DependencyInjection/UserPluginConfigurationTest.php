<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\DependencyInjection;

use Matthias\BundlePlugins\ConfigurationWithPlugins;
use Matthias\SymfonyConfigTest\PhpUnit\ConfigurationTestCaseTrait;
use ProjectBoston\CustomerServiceCenter\User\DependencyInjection\UserPlugin;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\DependencyInjection\UserPlugin
 */
class UserPluginConfigurationTest extends \PHPUnit_Framework_TestCase
{

    use ConfigurationTestCaseTrait;

    /**
     * @test
     */
    public function valuesAreInvalidIfInvalidFromAddressIsProvided()
    {
        $this->assertConfigurationIsInvalid(
            [['user' => []]],
            'from_address'
        );
    }

    /**
     * @test
     */
    public function valuesAreInvalidIfInvalidRolesIsProvided()
    {
        $this->assertConfigurationIsInvalid(
            [['user' => ['from_address' => 'testMail']]],
            'roles'
        );
    }

    /**
     * @test
     */
    public function processedValueContainsRequiredValue()
    {
        $this->assertProcessedConfigurationEquals(
            [
                [
                    'user' => [
                        'from_address' => 'testMail',
                        'roles'        => ['user' => [['alias' => 'dummy', 'role' => 'ROLE_DUMMY']]]
                    ]
                ]
            ],
            [
                'user' => [
                    'from_address' => 'testMail',
                    'roles'        => ['user' => ['dummy' => ['role' => 'ROLE_DUMMY']]]
                ]
            ]
        );
    }

    protected function getConfiguration()
    {
        return new ConfigurationWithPlugins('project_boston_customer_service_center', [new UserPlugin()]);
    }
}
