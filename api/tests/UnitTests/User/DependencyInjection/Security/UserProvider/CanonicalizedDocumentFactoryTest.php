<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\DependencyInjection\Security\UserProvider;

use ProjectBoston\CustomerServiceCenter\User\DependencyInjection\Security\UserProvider\CanonicalizedDocumentFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\DependencyInjection\Security\UserProvider\CanonicalizedDocumentFactory
 */
class CanonicalizedDocumentFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function create()
    {
        $container = new ContainerBuilder();
        $config = ['class' => 'class', 'property' => 'property', 'manager_name' => 'manager_name'];
        $documentFactory = new CanonicalizedDocumentFactory('key', 'provider');

        $documentFactory->create($container, 'test', $config);

        self::assertInstanceOf(Reference::class,$container->getDefinition('test')->getArgument(0));
    }
}
