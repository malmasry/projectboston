<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\DependencyInjection;

use Matthias\BundlePlugins\ExtensionWithPlugins;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use ProjectBoston\CustomerServiceCenter\User\DependencyInjection\UserPlugin;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\DependencyInjection\UserPlugin
 */
class UserPluginExtensionTest extends AbstractExtensionTestCase
{
    /**
     * @test
     */
    public function loadWithValidValues()
    {
        $this->container->setDefinition('projectboston.csc.user.object_roles', new Definition());

        $this->load();

        $this->assertContainerBuilderHasParameter('project_boston_csc.user.from_address', 'dummyMail');
        $this->assertContainerBuilderHasParameter(
            'project_boston_csc.user.alias_roles',
            ['user' => ['dummy']]
        );
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'projectboston.csc.user.object_roles',
            'add',
            [new Definition(Role::class, ['ROLE_DUMMY', 'dummy'])]
        );

    }

    /**
     * Return an array of container extensions you need to be registered for each test (usually just the container
     * extension you are testing.
     *
     * @return ExtensionInterface[]
     */
    protected function getContainerExtensions()
    {
        return [new ExtensionWithPlugins('project_boston_customer_service_center', [new UserPlugin()])];
    }

    /**
     * Optionally override this method to return an array that will be used as the minimal configuration for loading
     * the container extension under test, to prevent a test from failing because of a missing required
     * configuration value for the container extension.
     */
    protected function getMinimalConfiguration()
    {
        return [
            'user' => [
                'from_address' => 'dummyMail',
                'roles'        => ['user' => ['dummy' => ['role' => 'ROLE_DUMMY']]]
            ]
        ];
    }
}
