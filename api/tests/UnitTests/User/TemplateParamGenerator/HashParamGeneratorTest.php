<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\TemplateParamGenerator;

use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\HashParamGenerator;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\HashParamGenerator
 */
class HashParamGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getParameters()
    {
        $data      = new \stdClass();
        $user      = $this->prophesize(User::class);
        $generator = new HashParamGenerator();

        $user->getConfirmationToken()->willReturn('token');

        $data->baseUrl = 'url';

        self::assertEquals(['baseUrl' => 'url', 'hash' => 'token'], $generator->getParameters($data, $user->reveal()));
    }
}
