<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\TemplateParamGenerator;

use ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\TemplateParamGeneratorChain;
use ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\TemplateParamGeneratorInterface;
use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\TemplateParamGeneratorChain
 */
class TemplateParamGeneratorChainTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TemplateParamGeneratorChain
     */
    private $chain;
    private $paramGenerator;
    private $user;

    /**
     * @test
     */
    public function getParameters()
    {
        $data = (object)['type' => 1];

        $this->paramGenerator->getParameters($data, $this->user->reveal())->willReturn(['params']);

        self::assertEquals(['params'], $this->chain->getParameters($data, $this->user->reveal()));
    }

    /**
     * @test
     */
    public function getParametersForNotKnownType()
    {
        $data = (object)['type' => 2];

        self::assertEquals([], $this->chain->getParameters($data, $this->user->reveal()));
    }


    protected function setUp()
    {
        $this->paramGenerator = $this->prophesize(TemplateParamGeneratorInterface::class);
        $this->user       = $this->prophesize(User::class);
        $this->chain      = new TemplateParamGeneratorChain();

        $this->chain->addParamGenerator(1, $this->paramGenerator->reveal());
    }
}
