<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Validator\Constraints;

use ProjectBoston\CustomerServiceCenter\User\Model\SetPassword;
use ProjectBoston\CustomerServiceCenter\User\Validator\Constraints\ValidPassword;
use ProjectBoston\CustomerServiceCenter\User\Validator\Constraints\ValidPasswordValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 * 
 * @covers ProjectBoston\CustomerServiceCenter\User\Validator\Constraints\ValidPasswordValidator
 */
class ValidPasswordValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ValidPasswordValidator
     */
    private $validator;
    private $context;

    /**
     * @test
     */
    public function roleIsInvalid()
    {
        $passwordModel = $this->prophesize(SetPassword::class);

        $passwordModel->getPlainPassword()->willReturn('other');
        $passwordModel->getRepeatedPlainPassword()->willReturn('test');

        $this->context->addViolation(
            (new ValidPassword())->message,
            ['{{ plainPassword }}' => 'other'],
            'other'
        )->shouldBeCalled();

        $this->validator->validate($passwordModel->reveal(), new ValidPassword());
    }

    protected function setUp()
    {
        $this->context =  $this->prophesize(ExecutionContextInterface::class);
        $this->validator = new ValidPasswordValidator();
        $this->validator->initialize($this->context->reveal());
    }
}
