<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Validator\Constraints;

use Doctrine\Common\Collections\ArrayCollection;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;
use ProjectBoston\CustomerServiceCenter\User\Validator\Constraints\ValidRole;
use ProjectBoston\CustomerServiceCenter\User\Validator\Constraints\ValidRoleValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Validator\Constraints\ValidRoleValidator
 */
class ValidRoleValidatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ValidRoleValidator
     */
    private $validator;
    private $context;
    private $role;

    /**
     * @test
     */
    public function roleIsInvalid()
    {
        $this->role->getName()->willReturn('other');

        $this->context->addViolation(
            (new ValidRole())->message,
            ['{{ role }}' => 'other'],
            'other'
        )->shouldBeCalled();

        $this->validator->validate($this->role->reveal(), new ValidRole());
    }

    protected function setUp()
    {
        $this->role      = $this->prophesize(Role::class);
        $this->context   = $this->prophesize(ExecutionContextInterface::class);
        $this->validator = new ValidRoleValidator(new ArrayCollection([new Role('test', 'test')]));
        $this->validator->initialize($this->context->reveal());
    }
}
