<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\StateMachine\Callback;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\StateMachine\Callback\SetPassword;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\StateMachine\Callback\SetPassword
 */
class SetPasswordTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SetPassword
     */
    private $callback;
    private $passwordEncoder;
    private $dm;

    /**
     * @test
     */
    public function execute()
    {
        $userMock = $this->prophesize(User::class);

        $this->passwordEncoder->encodePassword('passwordRaw', null)->willReturn('passwordEncoded');

        $userMock->getPassword()->willReturn('passwordRaw');
        $userMock->setPassword('passwordEncoded')->shouldBeCalled();
        $userMock->setEnabled(true)->shouldBeCalled();
        $userMock->setConfirmationToken(null)->shouldBeCalled();

        $this->dm->flush()->shouldBeCalled();

        $this->callback->execute($userMock->reveal());
    }


    protected function setUp()
    {
        $this->passwordEncoder = $this->prophesize(PasswordEncoderInterface::class);
        $this->dm              = $this->prophesize(ObjectManager::class);
        $this->callback        = new SetPassword($this->passwordEncoder->reveal(), $this->dm->reveal());
    }
}
