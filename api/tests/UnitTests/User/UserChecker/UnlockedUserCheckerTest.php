<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\UserChecker;

use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\UserChecker\UnlockedUserChecker;
use Psr\Log\LoggerInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\UserChecker\UnlockedUserChecker
 */
class UnlockedUserCheckerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UnlockedUserChecker
     */
    private $checker;
    private $logger;
    private $user;

    /**
     * @test
     */
    public function checkWithNoUser()
    {
        $this->logger->error('User with ID "51e1eefc065f908c10000411" not found.')->shouldBeCalled();

        self::assertFalse($this->checker->checkUser((object)['id' => '51e1eefc065f908c10000411']));
    }

    /**
     * @test
     */
    public function checkWithLockedUser()
    {
        $this->user->isAccountNonLocked()->willReturn(false);

        $this->logger->error('User with ID "51e1eefc065f908c10000411" is locked.')->shouldBeCalled();

        self::assertFalse(
            $this->checker->checkUser((object)['id' => '51e1eefc065f908c10000411'], $this->user->reveal())
        );
    }

    /**
     * @test
     */
    public function checkWithUnlockedUser()
    {
        $this->user->isAccountNonLocked()->willReturn(true);

        self::assertTrue(
            $this->checker->checkUser((object)['id' => '51e1eefc065f908c10000411'], $this->user->reveal())
        );
    }

    protected function setUp()
    {
        $this->logger = $this->prophesize(LoggerInterface::class);
        $this->user = $this->prophesize(User::class);
        $this->checker = new UnlockedUserChecker($this->logger->reveal());
    }
}
