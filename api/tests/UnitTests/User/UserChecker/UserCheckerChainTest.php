<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\UserChecker;

use ProjectBoston\CustomerServiceCenter\User\UserChecker\UserCheckerChain;
use ProjectBoston\CustomerServiceCenter\User\UserChecker\UserCheckerInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\UserChecker\UserCheckerChain
 */
class UserCheckerChainTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserCheckerChain
     */
    private $chain;
    private $userChecker;

    /**
     * @test
     */
    public function checkUser()
    {
        $data = (object)['type' => 1];

        $this->userChecker->checkUser($data, null)->willReturn(true);

        self::assertTrue($this->chain->checkUser($data));
    }

    /**
     * @test
     */
    public function checkUserWithUnknowType()
    {
        $data = (object)['type' => 2];

        self::assertFalse($this->chain->checkUser($data));
    }

    protected function setUp()
    {
        $this->chain = new UserCheckerChain();
        $this->userChecker = $this->prophesize(UserCheckerInterface::class);
        $this->chain->addUserChecker(1, $this->userChecker->reveal());
    }
}
