<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\UserChecker;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\UserChecker\UserChecker;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Util\SecureRandomInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\UserChecker\UserChecker
 */
class UserCheckerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserChecker
     */
    private $checker;
    private $logger;
    private $user;
    private $hashGenerator;
    private $objectManager;

    /**
     * @test
     */
    public function checkWithNoUser()
    {
        $this->logger->error('User with ID "51e1eefc065f908c10000411" not found.')->shouldBeCalled();

        self::assertFalse($this->checker->checkUser((object)['id' => '51e1eefc065f908c10000411']));
    }

    /**
     * @test
     */
    public function checkWithEnabledUser()
    {
        $this->user->isEnabled()->willReturn(true);

        $this->logger->error('User with ID "51e1eefc065f908c10000411" was enabled.')->shouldBeCalled();

        self::assertFalse(
            $this->checker->checkUser((object)['id' => '51e1eefc065f908c10000411'], $this->user->reveal())
        );
    }

    /**
     * @test
     */
    public function checkWithUnlockedUser()
    {
        $this->user->isEnabled()->willReturn(false);
        $this->user->setConfirmationToken(str_replace(['+', '/', '='], ['-', '_', ','], base64_encode('test')))
            ->shouldBeCalled();

        $this->hashGenerator->nextBytes(32)->willReturn('test');

        $this->objectManager->flush()->shouldBeCalled();
        $this->objectManager->clear()->shouldBeCalled();

        self::assertTrue(
            $this->checker->checkUser((object)['id' => '51e1eefc065f908c10000411'], $this->user->reveal())
        );
    }

    protected function setUp()
    {
        $this->logger = $this->prophesize(LoggerInterface::class);
        $this->objectManager = $this->prophesize(ObjectManager::class);
        $this->hashGenerator = $this->prophesize(SecureRandomInterface::class);
        $this->user = $this->prophesize(User::class);
        $this->checker = new UserChecker(
            $this->objectManager->reveal(),
            $this->logger->reveal(),
            $this->hashGenerator->reveal()
        );
    }
}
