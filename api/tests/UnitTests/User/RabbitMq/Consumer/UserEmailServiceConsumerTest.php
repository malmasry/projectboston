<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\RabbitMq\Consumer;

use ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailerInterface;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\RabbitMq\Consumer\UserEmailServiceConsumer;
use ProjectBoston\CustomerServiceCenter\User\Repository\UserRepository;
use ProjectBoston\CustomerServiceCenter\User\UserChecker\UserCheckerInterface;
use Prophecy\Argument;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\RabbitMq\Consumer\UserEmailServiceConsumer
 */
class UserEmailServiceConsumerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserEmailServiceConsumer
     */
    private $serviceConsumer;
    private $repo;
    private $mailer;
    private $checker;
    private $data;
    private $user;

    /**
     * @test
     */
    public function checkUser()
    {
        $this->checker->checkUser($this->data, $this->user->reveal())->willReturn(false);

        self::assertEquals(-1, $this->serviceConsumer->process($this->data));
    }

    /**
     * @test
     */
    public function sendEmail()
    {
        $this->checker->checkUser($this->data, $this->user->reveal())->willReturn(true);
        $this->mailer->sendEmail($this->data, $this->user->reveal())->willReturn(1);

        self::assertEquals(1, $this->serviceConsumer->process($this->data));
    }

    /**
     * @test
     */
    public function sendNoEmail()
    {
        $this->checker->checkUser($this->data, $this->user->reveal())->willReturn(true);
        $this->mailer->sendEmail($this->data, $this->user->reveal())->willReturn(0);

        self::assertEquals(2, $this->serviceConsumer->process($this->data));
    }

    protected function setUp()
    {
        $this->user = $this->prophesize(User::class);
        $this->repo    = $this->prophesize(UserRepository::class);
        $this->mailer  = $this->prophesize(UserMailerInterface::class);
        $this->checker = $this->prophesize(UserCheckerInterface::class);
        $this->data = (object) ['id' => '51e1eefc065f908c10000411'];

        $this->repo->findUserById(Argument::type('MongoID'))->willReturn($this->user->reveal());

        $this->serviceConsumer = new UserEmailServiceConsumer(
            $this->repo->reveal(),
            $this->checker->reveal(),
            $this->mailer->reveal()
        );
    }
}
