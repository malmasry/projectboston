<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Mailer;

use ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailerChain;
use ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailerInterface;
use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailerChain
 */
class UserMailerChainTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserMailerChain
     */
    private $chain;
    private $userMailer;
    private $user;

    /**
     * @test
     */
    public function sendEmail()
    {
        $data = (object)['type' => 1];

        $this->userMailer->sendEmail($data, $this->user->reveal())->willReturn(1);

        self::assertEquals(1, $this->chain->sendEmail($data, $this->user->reveal()));
    }

    /**
     * @test
     */
    public function sendEmailForNotKnownType()
    {
        $data = (object)['type' => 2];

        self::assertEquals(0, $this->chain->sendEmail($data, $this->user->reveal()));
    }


    protected function setUp()
    {
        $this->userMailer = $this->prophesize(UserMailerInterface::class);
        $this->user       = $this->prophesize(User::class);
        $this->chain      = new UserMailerChain();

        $this->chain->addUserMailer(1, $this->userMailer->reveal());
    }
}
