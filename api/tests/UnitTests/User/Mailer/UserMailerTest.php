<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Mailer;

use ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailer;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\TemplateParamGenerator\TemplateParamGeneratorInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Mailer\UserMailer
 */
class UserMailerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserMailer
     */
    private $userMailer;
    private $mailer;
    private $translator;
    private $templating;
    private $paramGenerator;
    private $dataObject;
    private $user;

    /**
     * @test
     */
    public function sendMail()
    {
        $messageMock = $this->prophesize(\Swift_Message::class);
        $transportMock = $this->prophesize(\Swift_Transport::class);
        $transportMock->isStarted()->willReturn(false);
        $transportMock->start()->shouldBeCalled();
        $transportMock->stop()->shouldBeCalled();

        $this->user->getLocale()->willReturn('de');
        $this->user->getEmail()->willReturn('toAddress');

        $this->translator->setLocale('de')->shouldBeCalled();
        $this->translator->trans('subject', [], 'mail')->willReturn('transSubject');

        $this->templating->render('txtTemplatePath', ['params'])->willReturn('txtTemplate');
        $this->templating->render('htmlTemplatePath', ['params'])->willReturn('htmlTemplate');

        $this->paramGenerator->getParameters($this->dataObject, $this->user->reveal())->willReturn(['params']);

        $messageMock->setSubject('transSubject')->willReturn($messageMock->reveal());
        $messageMock->setFrom('fromAddress')->willReturn($messageMock->reveal());
        $messageMock->setTo('toAddress')->willReturn($messageMock->reveal());
        $messageMock->setBody('txtTemplate')->willReturn($messageMock->reveal());
        $messageMock->addPart('htmlTemplate', 'text/html')->willReturn($messageMock->reveal());

        $this->mailer->send($messageMock->reveal())->shouldBeCalled();
        $this->mailer->createMessage()->willReturn($messageMock->reveal());

        $this->mailer->getTransport()->willReturn($transportMock->reveal());

        $this->userMailer->sendEmail($this->dataObject, $this->user->reveal());
    }

    protected function setUp()
    {
        $this->mailer         = $this->prophesize(\Swift_Mailer::class);
        $this->translator     = $this->prophesize(TranslatorInterface::class);
        $this->templating     = $this->prophesize(EngineInterface::class);
        $this->paramGenerator = $this->prophesize(TemplateParamGeneratorInterface::class);
        $this->dataObject     = new \stdClass();
        $this->user           = $this->prophesize(User::class);

        $this->userMailer = new UserMailer(
            $this->mailer->reveal(),
            $this->translator->reveal(),
            $this->templating->reveal(),
            $this->paramGenerator->reveal(),
            ['txtTemplate' => 'txtTemplatePath', 'htmlTemplate' => 'htmlTemplatePath', 'subject' => 'subject', 'fromAddress' => 'fromAddress']
        );
    }
}
