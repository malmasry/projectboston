<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Serializer\Handler;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;
use ProjectBoston\CustomerServiceCenter\User\Model\Role;
use ProjectBoston\CustomerServiceCenter\User\Serializer\Handler\RoleHandler;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Serializer\Handler\RoleHandler
 */
class RoleHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var RoleHandler
     */
    private $handler;

    /**
     * @test
     */
    public function deserializeJsonToRole()
    {

        $visitor = $this->prophesize(JsonDeserializationVisitor::class);

        self::assertEquals(
            new Role('test', 'test'),
            $this->handler->deserializeJsonToRole($visitor->reveal(), 'test')
        );

        self::assertEquals(
            new Role('test2', 'test2'),
            $this->handler->deserializeJsonToRole($visitor->reveal(), 'test2')
        );
    }

    /**
     * @test
     */
    public function serializeJsonToRole()
    {
        $visitor = $this->prophesize(JsonSerializationVisitor::class);

        self::assertEquals('test', $this->handler->serializeRoleToJson($visitor->reveal(), new Role('test', 'test')));
    }

    protected function setUp()
    {
        $this->handler = new RoleHandler(new ArrayCollection([new Role('test', 'test')]));
    }
}
