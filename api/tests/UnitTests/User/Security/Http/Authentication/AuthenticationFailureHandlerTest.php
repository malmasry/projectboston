<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Security\Http\Authentication;

use ProjectBoston\CustomerServiceCenter\User\Security\Http\Authentication\AuthenticationFailureHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\LockedException;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Security\Http\Authentication\AuthenticationFailureHandler
 */
class AuthenticationFailureHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AuthenticationFailureHandler
     */
    private $handler;
    private $request;

    /**
     * @test
     */
    public function onAuthenticationFailureWithLockedException()
    {
        $response = $this->handler->onAuthenticationFailure(
            $this->request->reveal(),
            $this->prophesize(LockedException::class)->reveal()
        );

        self::assertTrue($response->headers->has('ProjectBoston-Reason'));
        self::assertEquals('locked', $response->headers->get('ProjectBoston-Reason'));
        self::assertEquals(401, $response->getStatusCode());
    }

    /**
     * @test
     */
    public function onAuthenticationFailure()
    {
        $response = $this->handler->onAuthenticationFailure(
            $this->request->reveal(),
            $this->prophesize(AuthenticationException::class)->reveal()
        );

        self::assertFalse($response->headers->has('ProjectBoston-Reason'));
        self::assertEquals(401, $response->getStatusCode());
    }

    protected function setUp()
    {
        $this->handler   = new AuthenticationFailureHandler();
        $this->request   = $this->prophesize(Request::class);
    }
}
