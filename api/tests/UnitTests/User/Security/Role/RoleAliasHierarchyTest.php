<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\Security\Role;

use Doctrine\Common\Collections\ArrayCollection;
use Documents\User;
use ProjectBoston\CustomerServiceCenter\User\Model\Role as UserRole;
use ProjectBoston\CustomerServiceCenter\User\Security\Role\RoleAliasHierarchy;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Security\Role\RoleAliasHierarchy
 */
class RoleAliasHierarchyTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var RoleAliasHierarchy
     */
    private $roleAliasHierarchy;
    private $roleHierarchy;
    private $roleObjects;

    /**
     * @test
     */
    public function getReachableRoles()
    {
        $roleArray = [
            new Role('ROLE_1'),
            new Role('ROLE_3')
        ];

        $this->roleHierarchy->getReachableRoles([])->willReturn($roleArray);

        $roles = $this->roleAliasHierarchy->getReachableRoles([]);
        self::assertCount(2, $roles);
        self::assertEquals('role_1', $roles[0]);
        self::assertEquals('role_3', $roles[1]);
    }

    /**
     * @test
     */
    public function getRoleHierarchyByAlias()
    {
        $result = $this->roleAliasHierarchy->getRoleHierarchyByAlias('role_2');

        self::assertCount(3, $result);
        self::assertEquals(['ROLE_2', 'ROLE_1', 'ROLE_3'], $result);

        self::assertCount(0, $this->roleAliasHierarchy->getRoleHierarchyByAlias('role_4'));
    }

    protected function setUp()
    {
        $this->roleHierarchy      = $this->prophesize(RoleHierarchyInterface::class);
        $this->roleObjects        = new ArrayCollection(
            [
                new UserRole('ROLE_1', 'role_1'),
                new UserRole('ROLE_2', 'role_2'),
                new UserRole('ROLE_3', 'role_3')
            ]
        );
        $this->roleAliasHierarchy = new RoleAliasHierarchy(
            $this->roleHierarchy->reveal(),
            $this->roleObjects,
            ['ROLE_1' => ['ROLE_2'], 'ROLE_3' => ['ROLE_1']]
        );
    }

}
