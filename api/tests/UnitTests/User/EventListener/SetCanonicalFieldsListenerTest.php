<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\EventListener;

use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use ProjectBoston\CustomerServiceCenter\User\Canonicalizer;
use ProjectBoston\CustomerServiceCenter\User\EventListener\SetCanonicalFieldsListener;
use ProjectBoston\CustomerServiceCenter\User\Model\User;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\Canonicalizer
 * @covers ProjectBoston\CustomerServiceCenter\User\EventListener\SetCanonicalFieldsListener
 */
class SetCanonicalFieldsListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SetCanonicalFieldsListener
     */
    private $eventListener;

    /**
     * @test
     */
    public function prePersistWithoutRightObject()
    {
        $event = $this->prophesize(LifecycleEventArgs::class);

        $event->getDocument()->willReturn('test');

        $this->eventListener->prePersist($event->reveal());
    }

    /**
     * @test
     */
    public function prePersist()
    {
        $event = $this->prophesize(LifecycleEventArgs::class);
        $userMock = $this->prophesize(User::class);

        $event->getDocument()->willReturn($userMock->reveal());

        $userMock->getEmail()->willReturn('MailMailMail');
        $userMock->setEmailCanonical('mailmailmail')->shouldBeCalled();

        $this->eventListener->prePersist($event->reveal());
    }

    protected function setUp()
    {
        $this->eventListener = new SetCanonicalFieldsListener(new Canonicalizer());
    }
}
