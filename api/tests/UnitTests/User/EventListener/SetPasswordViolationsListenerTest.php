<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\EventListener;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\User\EventListener\SetPasswordViolationsListener;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\EventListener\SetPasswordViolationsListener
 */
class SetPasswordViolationsListenerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function checkViolation()
    {
        $event      = $this->prophesize(FilterControllerEvent::class);
        $bag        = $this->prophesize(ParameterBagInterface::class);
        $violations = $this->prophesize(ConstraintViolationListInterface::class);
        $dm         = $this->prophesize(ObjectManager::class);
        $user       = $this->prophesize(User::class);

        $event->getRequest()->willReturn((object)['attributes' => $bag->reveal()]);

        $bag->get('violations')->willReturn($violations->reveal());
        $bag->get('_route')->willReturn('project_boston_csc_user_security_reset_password');
        $bag->get('user')->willReturn($user->reveal());

        $violations->count()->willReturn(1);

        $user->setConfirmationToken(null)->shouldBeCalled();

        $dm->flush()->shouldBeCalled();

        call_user_func(new SetPasswordViolationsListener($dm->reveal()), $event->reveal());
    }
}
