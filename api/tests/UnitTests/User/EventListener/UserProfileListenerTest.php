<?php

namespace ProjectBoston\CustomerServiceCenter\User\Tests\EventListener;

use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use JMS\Serializer\SerializationContext;
use ProjectBoston\CustomerServiceCenter\Review\Repository\ReviewRepository;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\User\EventListener\UserProfileListener;
use PhpCollection\Map;
use PhpOption\Some;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\User\EventListener\UserProfileListener
 */
class UserProfileListenerTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var UserProfileListener
     */
    private $listener;
    private $repository;

    /**
     * @test
     */
    public function preSerialize()
    {
        $event      = $this->prophesize(PreSerializeEvent::class);
        $context    = $this->prophesize(SerializationContext::class);
        $attributes = $this->prophesize(Map::class);
        $user       = $this->prophesize(User::class);

        $context->attributes = $attributes->reveal();

        $user->getId()->willReturn('id');
        $user->setReviewCount(2)->shouldBeCalled();

        $this->repository->countReviews('id')->willReturn(2);

        $event->getObject()->willReturn($user);
        $some = new Some(['profile']);

        $attributes->get('groups')->willReturn($some);

        $event->getContext()->willReturn($context->reveal());

        $this->listener->preSerialize($event->reveal());
    }

    protected function setUp()
    {
        $this->repository = $this->prophesize(ReviewRepository::class);

        $this->listener = new UserProfileListener($this->repository->reveal());
    }
}
