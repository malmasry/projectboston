<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Tests\Security;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Customer\Repository\CustomerRepository;
use ProjectBoston\CustomerServiceCenter\Customer\Security\CustomerAccessDecisionMaker;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\Customer\Security\CustomerAccessDecisionMaker
 */
class CustomerAccessDecisionMakerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CustomerAccessDecisionMaker
     */
    private $customerAccessDecisionMaker;
    private $customer;
    private $customerRepository;

    /**
     * @test
     */
    public function allowAccessIfNoCustomerIdIsSet()
    {
        self::assertTrue($this->customerAccessDecisionMaker->hasAccess($this->customer->reveal(), null));
    }

    /**
     * @test
     */
    public function allowAccessIfCustomerIdIsSameAsCurrent()
    {
        self::assertTrue($this->customerAccessDecisionMaker->hasAccess($this->customer->reveal(), '42'));
    }

    /**
     * @test
     */
    public function denyAccessIfNoCustomerForIdExists()
    {
        $this->customerRepository->findById('3')->willReturn(null);

        self::assertFalse($this->customerAccessDecisionMaker->hasAccess($this->customer->reveal(), '3'));
    }

    /**
     * @test
     */
    public function denyAccessIfCustomerHasNoAncestors()
    {
        $customer = $this->prophesize(Customer::class);

        $customer->getAncestors()->willReturn([]);

        $this->customerRepository->findById('3')->willReturn($customer->reveal());

        self::assertFalse($this->customerAccessDecisionMaker->hasAccess($this->customer->reveal(), '3'));
    }

    /**
     * @test
     */
    public function denyAccessIfCustomerHasNoAncestorsWithCorrectId()
    {
        $customer = $this->prophesize(Customer::class);

        $customer->getId()->willReturn(23);
        $customer->getAncestors()->willReturn([$customer->reveal()]);

        $this->customerRepository->findById('3')->willReturn($customer->reveal());

        self::assertFalse($this->customerAccessDecisionMaker->hasAccess($this->customer->reveal(), '3'));
    }

    /**
     * @test
     */
    public function allowAccessIfCustomerHasAncestorsWithCorrectId()
    {
        $customer = $this->prophesize(Customer::class);

        $customer->getId()->willReturn(42);
        $customer->getAncestors()->willReturn([$customer->reveal()]);

        $this->customerRepository->findById('3')->willReturn($customer->reveal());

        self::assertTrue($this->customerAccessDecisionMaker->hasAccess($this->customer->reveal(), '3'));
    }


    protected function setUp()
    {
        $this->customer = $this->prophesize(Customer::class);
        $this->customerRepository = $this->prophesize(CustomerRepository::class);
        $this->customerAccessDecisionMaker = new CustomerAccessDecisionMaker($this->customerRepository->reveal());

        $this->customer->getId()->willReturn(42);
    }
}
