<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Tests\Doctrine\MongoDB\Types;

use Doctrine\ODM\MongoDB\Types\Type;
use libphonenumber\PhoneNumber;
use ProjectBoston\CustomerServiceCenter\Customer\Doctrine\MongoDB\Types\PhoneNumberType;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class PhoneNumberTypeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var PhoneNumberType
     */
    private $type;

    /**
     * @test
     */
    public function convertToPHPValue()
    {
        self::assertNull($this->type->convertToPHPValue(null));

        self::assertInstanceOf(PhoneNumber::class, $this->type->convertToPHPValue('+41446681800'));
    }

    /**
     * @test
     */
    public function closureToPHP()
    {
        self::assertEquals(
            '$return = \libphonenumber\PhoneNumberUtil::getInstance()->parse($value, \libphonenumber\PhoneNumberUtil::UNKNOWN_REGION);',
            $this->type->closureToPHP()
        );
    }

    /**
     * @test
     */
    public function convertToDatabaseValueWithInt()
    {
        self::assertEquals('+494469852850', $this->type->convertToDatabaseValue('+494469852850'));
    }

    /**
     * @test
     */
    public function convertToDatabaseValue()
    {
        self::assertNull($this->type->convertToDatabaseValue(null));

        $phoneNumber = new PhoneNumber();
        $phoneNumber->setCountryCode('41');
        $phoneNumber->setNationalNumber('446681800');

        self::assertEquals('+41446681800', $this->type->convertToDatabaseValue($phoneNumber));
    }

    protected function setUp()
    {
        Type::registerType('phoneNumber', PhoneNumberType::class);
        $this->type = Type::getType('phoneNumber');
    }
}
