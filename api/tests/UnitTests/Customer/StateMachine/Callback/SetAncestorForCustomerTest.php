<?php

namespace ProjectBoston\Bundle\CustomerServiceCenter\CustomerBundle\Tests\StateMachine\Callback;

use Doctrine\Common\Collections\Collection;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Customer\StateMachine\Callback\SetAncestorForCustomer;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\Customer\StateMachine\Callback\SetAncestorForCustomer
 */
class SetAncestorForCustomerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var SetAncestorForCustomer
     */
    private $listener;

    /**
     * @test
     */
    public function setAncestorsForCustomer()
    {
        $objMock = $this->prophesize(Customer::class);
        $ancestorsMock = $this->prophesize(Collection::class);

        $ancestorsMock->toArray()->willReturn(['value1', 'value2']);

        $objMock->getParent()->willReturn($objMock->reveal());
        $objMock->getAncestors()->willReturn($ancestorsMock->reveal());
        $objMock->setAncestors(['value1', 'value2', $objMock->reveal()])->shouldBeCalled();

        $this->listener->execute($objMock->reveal());
    }

    /**
     * @test
     */
    public function setNoAncestorsForCustomer()
    {
        $objMock = $this->prophesize(Customer::class);
        $objMock->getParent()->willReturn(null);


        $this->listener->execute($objMock->reveal());
    }

    protected function setUp()
    {
        $this->listener = new SetAncestorForCustomer();
    }
}
