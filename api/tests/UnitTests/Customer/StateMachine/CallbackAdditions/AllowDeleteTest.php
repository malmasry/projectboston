<?php

namespace ProjectBoston\Bundle\CustomerServiceCenter\CustomerBundle\Tests\StateMachine\CallbackAdditions;

use Doctrine\Common\Persistence\ObjectManager;
use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;
use ProjectBoston\CustomerServiceCenter\Customer\Repository\CustomerRepository;
use ProjectBoston\CustomerServiceCenter\Review\Model\ObjectReview;
use ProjectBoston\CustomerServiceCenter\User\Model\User;
use ProjectBoston\CustomerServiceCenter\Customer\StateMachine\CallbackAdditions\AllowDelete;
use ProjectBoston\CustomerServiceCenter\User\Repository\UserRepository;
use Prophecy\Argument;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 *
 * @covers ProjectBoston\CustomerServiceCenter\Customer\StateMachine\CallbackAdditions\AllowDelete
 */
class AllowDeleteTest extends \PHPUnit_Framework_TestCase
{
    private $dm;
    private $userRepository;
    private $customerRepository;

    /**
     * @var AllowDelete
     */
    private $callback;

    /**
     * @test
     */
    public function allow()
    {
        $objectReview = $this->prophesize(ObjectReview::class);
        $userMock = $this->prophesize(User::class);
        $customerMock = $this->prophesize(Customer::class);

        $customerMock->getId()->willReturn(1);

        $objectReview->getObject()->willReturn($customerMock->reveal());

        $affectedCustomerIds = [1, 3, 4, 5, 6, 7, 8];

        $affectedCustomerMock = $this->prophesize(Customer::class);
        $affectedCustomerMock->getId()->willReturn(1, 3, 4, 5, 6, 7, 8);
        $affectedCustomers = [
            $affectedCustomerMock->reveal(),
            $affectedCustomerMock->reveal(),
            $affectedCustomerMock->reveal(),
            $affectedCustomerMock->reveal(),
            $affectedCustomerMock->reveal(),
            $affectedCustomerMock->reveal(),
            $affectedCustomerMock->reveal(),
        ];

        $this->customerRepository->findAllDeletableCustomer(1)->willReturn($affectedCustomers);

        # delete users call
        $this->userRepository->findAllUsersByCustomerIds($affectedCustomerIds)->willReturn([$userMock->reveal()]);

        # delete customers call
        $this->dm->remove(Argument::type(Customer::class))->shouldBeCalled();
        $this->dm->remove($userMock->reveal())->shouldBeCalled();
        $this->dm->flush()->shouldBeCalled();

        $this->callback->allow($objectReview->reveal());
    }

    protected function setUp()
    {
        $this->customerRepository = $this->prophesize(CustomerRepository::class);
        $this->userRepository = $this->prophesize(UserRepository::class);
        $this->dm = $this->prophesize(ObjectManager::class);
        $this->callback = new AllowDelete(
            $this->customerRepository->reveal(),
            $this->userRepository->reveal(),
            $this->dm->reveal()
        );
    }
}
