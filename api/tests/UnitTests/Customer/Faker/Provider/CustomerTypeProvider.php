<?php

namespace ProjectBoston\CustomerServiceCenter\Customer\Tests\Faker\Provider;

use ProjectBoston\CustomerServiceCenter\Customer\Model\Customer;

/**
 * @author Mohamed Almasry <almasry@almasry.ws>
 */
class CustomerTypeProvider
{
    /**
     * @return array
     */
    public function typeName()
    {
        $names = array(
            Customer::TYPE_ADMIN,
            Customer::TYPE_CONSUMER,
            Customer::TYPE_PARTNER
        );

        return $names[array_rand($names)];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return 'customer_type';
    }
}
