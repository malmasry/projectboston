# ProjectBoston Customer Service Center API

[![Build Status](https://jenkins-pe.projectboston.com/buildStatus/icon?job=projectboston-csc-api)](https://jenkins-pe.projectboston.com/view/CSC/job/projectboston-csc-api/)

... based on Symfony2


## Installation

Following commands should be executed on the vagrant machine (`vagrant ssh`).


### Generate SSH Keys:

    cd project/api
    mkdir -p app/var/jwt
    openssl genrsa -out app/var/jwt/private.pem -aes256 4096
    openssl rsa -pubout -in app/var/jwt/private.pem -out app/var/jwt/public.pem

### Install required components

    bin/composer install

If you been asked for a token, follow the url provided in the console and generate it with presetted defaults.

Parameters you should asked for during composing are following:

    database_name: customer_service_center
    database_server: 'mongodb://mongo03.mongo.dev.csc:27017'
    database_options: {  }
    mailer_transport: smtp
    mailer_host: mailcatcher.mailcatcher.dev.csc
    mailer_user: null
    mailer_password: null
    mailer_port: 1025
    user_from_address: csc@projectboston.com
    locale: en
    secret: ThisTokenIsNotSoSecretChangeIt
    rabbit_mq_user: guest
    rabbit_mq_password: guest
    rabbit_mq_host: rabbitmq.rabbitmq.dev.csc
    rabbit_mq_vhost: /
    guzzle_verify: true
    csc_host: csc.projectboston.dev
    nginx_host: nginx.nginx.dev.csc

## Testing and Development

Prepare testing and development with the following steps:


### Create all Exchanges and Queues for RabbitMQ

    cd project/api
    bin/php bin/console rabbitmq:setup-fabric

### Start the message consumer

    cd project/api
    bin/php bin/console rabbitmq:consumer -w send_user_mail

or some shortcuts    
    
    bin/startMQs
    bin/startAllMQProducers
    bin/startAllMQConsumer

### Create some dummy data

    cd project/api
    bin/php bin/console h4cc_alice_fixtures:load:sets tests/behat/fixtures/AppSet.php

    # email: test@csc.projectboston.dev
    # password: test
     
    # Admin Customer
    # email: Admin@csc.projectboston.dev
    # password: test
     
    # Partner customer
    # email: Partner@csc.projectboston.dev
    # password: test
     
    # Consumer customer
    # email: Consumer@csc.projectboston.dev
    # password: test


### behat tests

    cd project/api
    bin/php bin/behat -c behat.yml.dist

### phpunit tests

    cd project/api
    bin/php bin/phpunit -c app/ src/

to debug tests prefix the command with xdebug ... or do it like this

    bin/php bash
    xdebug bin/behat -c app/behat.yml.dist
    xedbug bin/phpunit -c app/ src/
