# ProjectBoston Customer Service Center

This repository contains a vagrant box that contains the ProjectBoston Customer Service Center for the EsnaManager Project and future products of ProjectBoston.

## Development

This repository is prepared so that when the following requirements are met, everything is provided for a local development environment:

### Requirements for Development

* [git](http://git-scm.com/ "git") min. version 1.7.x
* [Vagrant](http://www.vagrantup.com/ "Vagrant") min. version 1.6.0
* [VirtualBox](https://www.virtualbox.org/ "VirtualBox") min. version 4.3.6


#### Requirements for Development on Ubuntu

* In your PC or MAC BIOS settings, virtualization must be activated within the chipset settings
* The order of installation of Vagrant and VirtualBox should be adhered to, otherwise you will be given a Warning messages in which it is pointed out that Vagrant can only be installed if VirtualBox is previously uninstalled.
* NFS must be installed. This can be done with the following command:

    `sudo apt-get install nfs-kernel-server`


## Installation & Configuration

First you should clone the working version of the Customer Service Center of ProjectBoston to the local development environment via `git clone` :

    git clone git@bitbucket.org:malmasry/projectboston.git

For operation with vagrant, the local `etc/hosts` must be extended by the following line:

    192.101.101.101 csc.projectboston.dev mongo02.csc.projectboston.dev mongo03.csc.projectboston.dev mongo01.csc.projectboston.dev rabbit.csc.projectboston.dev mailcatcher.csc.projectboston.dev mongoadmin.csc.projectboston.dev

Afterwards, start the virtual machine with the following command:

    cd vagrant/
    vagrant up

Then you have to connect to the virtual machine for the application:

    vagrant ssh

... and can start the project from there:

    cd project
    bin/start


### Installation for Development - UI

See [README] (ui) in the subdirectory 'ui'.


### Installation for Development - API

See [README] (api) in the subdirectory 'api'.


## Internal Links & Tools

 * [UI](https://csc.projectboston.dev/ "UI")
 * [API](https://csc.projectboston.dev/api/ "API")
 * [Rockmongo](http://mongoadmin.csc.projectboston.dev/ "Rockmongo")
 * [Mailcatcher](http://csc.projectboston.dev:1080 "Mailcatcher")
 * [RabbitMQ](http://rabbit.csc.projectboston.dev/ "Rabbit MQ")
 * [Rest API Documentation](https://csc.projectboston.dev/api/doc/ "Rest API Documentation")


## Links

* [Repository](https://bitbucket.org/ProjectBoston/projectbostonCustomerServiceCenter "Repository")
* [Jenkins](https://jenkins-pe.projectboston.com/view/CSC/ "Jenkins")
* [Wiki Projekt](http://wiki.projectboston.com/display/it/CustomerServiceCenter "Wiki Projekt")
* [Wiki Vagrant](http://wiki.projectboston.com/display/it/Vagrant "Wiki Vagrant")
