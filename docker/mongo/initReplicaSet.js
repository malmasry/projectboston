conf = {
    "_id": "dev",
    "version": 1,
    "members": [
        {
            "_id": 0,
            "host": "mongo03.mongo.dev.csc:27017"
        },
        {
            "_id": 1,
            "host": "mongo02.mongo.dev.csc:27017"
        },
        {
            "_id": 2,
            "host": "mongo01.mongo.dev.csc:27017"
        }
    ]
}


rs.initiate(conf)